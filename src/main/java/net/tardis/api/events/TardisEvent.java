package net.tardis.api.events;

import net.minecraftforge.eventbus.api.Cancelable;
import net.minecraftforge.eventbus.api.Event;
import net.minecraftforge.eventbus.api.Event.HasResult;
import net.tardis.mod.tileentities.ConsoleTile;
/** Custom events that allow modders to intercept and handle their own logic
 * <p>This event is {@linkplain net.minecraftforge.eventbus.api.Cancelable cancellable},
 * and does not {@linkplain HasResult have a result}.</p>
 *
 * <p>This event fired in {@link ConsoleTile} class, on the
 * {@link net.minecraftforge.common.MinecraftForge#EVENT_BUS} only on the
 * {@linkplain net.minecraftforge.fml.LogicalSide#SERVER server-side}.
 * */
@Cancelable
public class TardisEvent extends Event{

	ConsoleTile tile;
	
	public TardisEvent(ConsoleTile tile) {
		this.tile = tile;
	}
	
	public ConsoleTile getConsole() {
		return tile;
	}
	/** This event allows modders to modify Tardis behaviour after the Tardis has "taken off". 
	 * <p> It is fired after the Tardis exterior blocks have been removed from the start position
	 * <br> The exterior chunk is force loaded at this time
	 * 
	 * <p>This event is {@linkplain net.minecraftforge.eventbus.api.Cancelable cancellable},
     * and does not {@linkplain HasResult have a result}.
     * 
     * <p>This event fired in {@link ConsoleTile#takeoff()}, on the
     * {@link net.minecraftforge.common.MinecraftForge#EVENT_BUS} only on the
     * {@linkplain net.minecraftforge.fml.LogicalSide#SERVER server-side}.
    */
	public static class Takeoff extends TardisEvent{

		public Takeoff(ConsoleTile tile) {
			super(tile);
		}
		
	}
	/** This event allows modders to modify Tardis behaviour after it has "landed"
	 * <p> It is fired after the Tardis has fully landed. 
	 * <p> At this point the exterior blocks have been placed, but the destination chunk are still force loaded
	 * * <p>This event fired in {@link ConsoleTile#land()}, on the
     * {@link net.minecraftforge.common.MinecraftForge#EVENT_BUS} only on the
     * {@linkplain net.minecraftforge.fml.LogicalSide#SERVER server-side}.
	 * */
	public static class Land extends TardisEvent{

		public Land(ConsoleTile tile) {
			super(tile);
		}
		
	}
}
