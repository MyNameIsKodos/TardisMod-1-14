package net.tardis.mod.client;

import java.util.List;
import java.util.Set;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.fml.network.NetworkEvent.Context;
import net.tardis.mod.boti.IBotiEnabled;
import net.tardis.mod.boti.WorldShell;
import net.tardis.mod.boti.stores.EntityStorage;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.guis.IGetStructures;
import net.tardis.mod.client.guis.INeedTardisNames;
import net.tardis.mod.client.guis.monitors.WaypointMonitorScreen;
import net.tardis.mod.enums.EnumMatterState;
import net.tardis.mod.misc.BrokenExteriorType;
import net.tardis.mod.missions.MiniMission;
import net.tardis.mod.network.packets.BOTITileMessage;
import net.tardis.mod.network.packets.BrokenTardisParticleSpawn;
import net.tardis.mod.network.packets.MissionUpdateMessage;
import net.tardis.mod.network.packets.SyncDimensionListMessage;
import net.tardis.mod.network.packets.TardisNameGuiMessage;
import net.tardis.mod.network.packets.TelepathicGetStructuresMessage;
import net.tardis.mod.network.packets.WaypointOpenMessage;
import net.tardis.mod.network.packets.exterior.ExteriorData;
import net.tardis.mod.registries.MissionRegistry;
import net.tardis.mod.tileentities.BrokenExteriorTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

/**Stores Client Side Network Packet methods that can be called inside a packet*/
public class ClientPacketHandler {
	
    public static void handleClientSideNames(TardisNameGuiMessage mes) {
		Screen s = Minecraft.getInstance().currentScreen;
		if(s instanceof INeedTardisNames)
			((INeedTardisNames)s).setNamesFromServer(mes.getNames());
	}
    
    public static void handleTelepathicStructureNames(TelepathicGetStructuresMessage mes) {
    	Screen s = Minecraft.getInstance().currentScreen;
    	if (s instanceof IGetStructures) {
    		IGetStructures screen = (IGetStructures)s;
    		screen.setTelepathicStructureNamesFromServer(mes.getSearchNames());
    	}
    }
    
    public static PlayerEntity getPlayer(Context context) {
		if(context.getDirection() == NetworkDirection.PLAY_TO_SERVER)
			return context.getSender();
		return DistExecutor.callWhenOn(Dist.CLIENT, () -> () -> Minecraft.getInstance().player);
    }
    
    public static void handleWorldShellEntity(int type, List<EntityStorage> entities, BlockPos pos) {
    	//The TE is always null for some reason, so comment out for now if needed
    	if(type == 0) {
			TileEntity te = Minecraft.getInstance().world.getTileEntity(pos);
			if(te instanceof IBotiEnabled) {
				IBotiEnabled boti = (IBotiEnabled)te;
				
				if(boti.getBotiWorld() != null)
					boti.getBotiWorld().updateEntities(entities);
				
			}
		}
		else {
			Minecraft.getInstance().world.getCapability(Capabilities.TARDIS_DATA).ifPresent(tile -> {
				if(tile.getBotiWorld() != null) {
					tile.getBotiWorld().updateEntities(entities);
				}
			});
		}
    }
    
    public static void handleBOTIMessage(BlockPos pos, WorldShell shell) {
    	//Handle TE- specific shells, like exteriors
		if(pos != null) {
			TileEntity te = Minecraft.getInstance().world.getTileEntity(pos);
			if(te instanceof IBotiEnabled)
				combineOrSetBOTI((IBotiEnabled)te, shell);
		}
		//Handle world capability shell, or monitors or interiors
		else {
			Minecraft.getInstance().world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
				combineOrSetBOTI(data, shell);
			});
		}
    }
    
    public static void combineOrSetBOTI(IBotiEnabled boti, WorldShell shell) {
		
		if(boti.getBotiWorld() == null || !boti.getBotiWorld().getOffset().equals(shell.getOffset())) {
			boti.setBotiWorld(shell);
			shell.setNeedsUpdate(true);
			return;
		}
		
		if(boti.getBotiWorld().getOffset().equals(shell.getOffset())) {
			boti.getBotiWorld().combine(shell);
		}
		
	}
    
    public static void syncDimensionList(SyncDimensionListMessage packet) {
    	@SuppressWarnings("resource")
		ClientPlayerEntity player = Minecraft.getInstance().player;
		RegistryKey<World> key = packet.getId();
		if (player == null || key == null)
			return;
		Set<RegistryKey<World>> worlds = player.connection.getDimensionKeys();
		if (worlds == null)
			return;
		if (packet.getAdd()){
			worlds.add(key);
		}
		else{
			worlds.remove(key);
		}
    }
    
    public static void handleTardisParticleSpawn(BrokenTardisParticleSpawn mes) {
		TileEntity te = Minecraft.getInstance().world.getTileEntity(mes.getPos());
		if(te instanceof BrokenExteriorTile) {
			BrokenExteriorType type = ((BrokenExteriorTile)te).getBrokenType();
			type.spawnHappyPart(te.getWorld(), mes.getPos());
		}
    }
    
    public static void handleWaypointOpenClient(WaypointOpenMessage mes) {
		Screen screen = Minecraft.getInstance().currentScreen;
		if(screen instanceof WaypointMonitorScreen);
			((WaypointMonitorScreen)screen).setWaypoints(mes.getWaypointMap());
    }
    
    public static void handleMissionUpdateClient(MissionUpdateMessage mes) {
		Minecraft.getInstance().world.getCapability(Capabilities.MISSION).ifPresent(misCap -> {
			MiniMission mis = misCap.getMissionForPos(mes.getPos());
			if(mis != null)
				mis.deserializeNBT(mes.getNBT());
			else {
				mis = MissionRegistry.MISSION_REGISTRY.get().getValue(mes.getKey()).create(Minecraft.getInstance().world, mes.getPos(), mes.getRange());
				mis.deserializeNBT(mes.getNBT());
				misCap.addMission(mis);
			}
		});
    }

    public static void handleBotiTileMessage(BOTITileMessage mes) {
    	World world = Minecraft.getInstance().world;

    	//If this is a TE that has BOTI
    	if(mes.pos != null){
			TileEntity boti = world.getTileEntity(mes.pos);
			if(boti instanceof IBotiEnabled){
				WorldShell shell = ((IBotiEnabled)boti).getBotiWorld();
				if(shell != null){
					shell.addTile(mes.store.pos, mes.store.createTile(shell.getWorld()));
				}
			}
		}
    	//If headed to the world cap
		else{
			world.getCapability(Capabilities.TARDIS_DATA).ifPresent(cap -> {
				if(cap.getBotiWorld() != null){
					cap.getBotiWorld().addTile(mes.store.pos, mes.store.createTile(cap.getBotiWorld().getWorld()));
				}
			});
		}

    }

    public static void handleMaterializationPacket(BlockPos pos, EnumMatterState state, int materializeTime) {
    	TileEntity te = Minecraft.getInstance().world.getTileEntity(pos);
    	if(te instanceof ExteriorTile){
    		ExteriorTile exterior = (ExteriorTile) te;
    		if(state == EnumMatterState.DEMAT)
    			exterior.demat(materializeTime);
    		else if (state == EnumMatterState.REMAT) //Explicit check for remat state. Otherwise we will use remat behaviour for SOLID state, which will always set the door to closed.
    			exterior.remat(materializeTime);
    		exterior.setMaterializeTime(materializeTime);

//    		System.out.println("Animation " + state + " with time " + materializeTime);
		}
    }

	public static void handleExteriorData(BlockPos pos, ExteriorData data) {
    	if(Minecraft.getInstance().world != null){
    		TileEntity te = Minecraft.getInstance().world.getTileEntity(pos);
    		if(te instanceof ExteriorTile){
				data.apply((ExteriorTile) te);
			}
		}
	}
}
