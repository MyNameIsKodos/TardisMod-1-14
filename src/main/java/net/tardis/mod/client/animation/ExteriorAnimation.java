package net.tardis.mod.client.animation;

import net.minecraft.nbt.IntNBT;
import net.tardis.mod.enums.EnumMatterState;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public abstract class ExteriorAnimation implements IExteriorAnimation{
	
	protected ExteriorTile exterior;
	private ExteriorAnimationEntry type;
	private int materializationTimer;

	public ExteriorAnimationEntry getType(){
		return type;
	}

    public ExteriorAnimation(ExteriorAnimationEntry entry, ExteriorTile tile) {
		this.type = entry;
		this.exterior = tile;
	}

	@Override
	public void startAnim(EnumMatterState state, int dematTime) {
		this.materializationTimer = dematTime;
	}

	public int getMaxTime(){
		return this.materializationTimer;
	}

	@Override
	public IntNBT serializeNBT() {
		return IntNBT.valueOf(this.materializationTimer);
	}

	@Override
	public void deserializeNBT(IntNBT nbt) {
		this.materializationTimer = nbt.getInt();
	}


}