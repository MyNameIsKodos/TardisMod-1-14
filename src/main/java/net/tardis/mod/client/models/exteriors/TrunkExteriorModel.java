package net.tardis.mod.client.models.exteriors;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.util.text.TextFormatting;
import net.tardis.mod.client.models.TextLabelModelRenderer;
import net.tardis.mod.client.renderers.boti.BOTIRenderer;
import net.tardis.mod.client.renderers.boti.PortalInfo;
import net.tardis.mod.client.renderers.exteriors.BrokenExteriorRenderer.IBrokenExteriorModel;
import net.tardis.mod.client.renderers.exteriors.ExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.TrunkExteriorRenderer;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.IDoorType;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class TrunkExteriorModel extends ExteriorModel implements IBrokenExteriorModel{
    private static WorldText TEXT_RENDERER = new WorldText(0.8F, 0.3F, 1, TextFormatting.BLACK);
    private String name = "";
    private ResourceLocation texture;
    private final TextLabelModelRenderer textRenderer;
    
    private final ModelRenderer box;
    private final ModelRenderer walls;
    private final ModelRenderer back_wall;
    private final ModelRenderer sidewall_right;
    private final ModelRenderer sidewall_left;
    private final ModelRenderer top_wall;
    private final ModelRenderer bottom_wall;
    private final ModelRenderer trim;
    private final ModelRenderer strap;
    private final ModelRenderer leather;
    private final ModelRenderer buckle;
    private final ModelRenderer corners;
    private final ModelRenderer knobs1;
    private final ModelRenderer knobs2;
    private final ModelRenderer knobs3;
    private final ModelRenderer knobs4;
    private final ModelRenderer knobs5;
    private final ModelRenderer knobs6;
    private final ModelRenderer knobs7;
    private final ModelRenderer knobs8;
    private final ModelRenderer hinge;
    private final ModelRenderer mount;
    private final ModelRenderer latch;
    private final ModelRenderer lock;
    private final ModelRenderer sticker1;
    private final ModelRenderer sticker2;
    private final ModelRenderer lid_rotate_y;
    private final ModelRenderer walls2;
    private final ModelRenderer back_wall2;
    private final ModelRenderer sidewall_right2;
    private final ModelRenderer sidewall_left2;
    private final ModelRenderer top_wall2;
    private final ModelRenderer bottom_wall2;
    private final ModelRenderer trim2;
    private final ModelRenderer corners2;
    private final ModelRenderer knobs9;
    private final ModelRenderer knobs10;
    private final ModelRenderer knobs11;
    private final ModelRenderer knobs12;
    private final ModelRenderer knobs13;
    private final ModelRenderer knobs14;
    private final ModelRenderer knobs15;
    private final ModelRenderer knobs16;
    private final ModelRenderer mount2;
    private final ModelRenderer latch2;
    private final ModelRenderer sticker3;
    private final ModelRenderer sticker4;
    private final ModelRenderer boti;

    public TrunkExteriorModel() {
        textureWidth = 256;
        textureHeight = 256;

        box = new ModelRenderer(this);
        box.setRotationPoint(0.0F, 24.0F, 0.0F);
        

        walls = new ModelRenderer(this);
        walls.setRotationPoint(0.0F, 0.0F, 0.0F);
        box.addChild(walls);
        

        back_wall = new ModelRenderer(this);
        back_wall.setRotationPoint(0.0F, 0.0F, 0.0F);
        walls.addChild(back_wall);
        back_wall.setTextureOffset(160, 105).addBox(-16.0F, -65.0F, 14.9F, 32.0F, 64.0F, 1.0F, 0.0F, false);

        sidewall_right = new ModelRenderer(this);
        sidewall_right.setRotationPoint(0.0F, 0.0F, 0.0F);
        walls.addChild(sidewall_right);
        sidewall_right.setTextureOffset(175, 96).addBox(15.0F, -64.0F, -3.0F, 1.0F, 63.0F, 18.0F, 0.0F, false);

        sidewall_left = new ModelRenderer(this);
        sidewall_left.setRotationPoint(0.0F, 0.0F, 0.0F);
        walls.addChild(sidewall_left);
        sidewall_left.setTextureOffset(141, 87).addBox(-16.0F, -65.0F, -3.0F, 1.0F, 63.0F, 18.0F, 0.0F, false);

        top_wall = new ModelRenderer(this);
        top_wall.setRotationPoint(0.0F, 0.0F, 0.0F);
        walls.addChild(top_wall);
        top_wall.setTextureOffset(110, 98).addBox(-16.0F, -65.0F, -4.0F, 32.0F, 1.0F, 19.0F, 0.0F, false);

        bottom_wall = new ModelRenderer(this);
        bottom_wall.setRotationPoint(0.0F, 0.0F, 0.0F);
        walls.addChild(bottom_wall);
        bottom_wall.setTextureOffset(142, 160).addBox(-16.0F, -2.0F, -4.0F, 32.0F, 1.0F, 18.0F, 0.0F, false);

        trim = new ModelRenderer(this);
        trim.setRotationPoint(0.0F, 0.0F, 0.0F);
        box.addChild(trim);
        trim.setTextureOffset(68, 1).addBox(-16.5F, -64.0F, 14.5F, 2.0F, 62.0F, 2.0F, 0.0F, false);
        trim.setTextureOffset(70, 1).addBox(14.5F, -64.0F, 14.5F, 2.0F, 62.0F, 2.0F, 0.0F, false);
        trim.setTextureOffset(74, 5).addBox(-15.0F, -65.5F, 14.5F, 30.0F, 2.0F, 2.0F, 0.0F, false);
        trim.setTextureOffset(82, 3).addBox(-15.0F, -2.5F, 14.5F, 30.0F, 2.0F, 2.0F, 0.0F, false);
        trim.setTextureOffset(99, 42).addBox(14.5F, -2.5F, -3.0F, 2.0F, 2.0F, 18.0F, 0.0F, false);
        trim.setTextureOffset(96, 40).addBox(-16.5F, -2.5F, -3.0F, 2.0F, 2.0F, 18.0F, 0.0F, false);
        trim.setTextureOffset(104, 34).addBox(-16.5F, -65.5F, -3.0F, 2.0F, 2.0F, 18.0F, 0.0F, false);
        trim.setTextureOffset(102, 34).addBox(14.5F, -65.5F, -3.0F, 2.0F, 2.0F, 18.0F, 0.0F, false);
        trim.setTextureOffset(68, 3).addBox(14.5F, -62.0F, -4.5F, 2.0F, 58.0F, 2.0F, 0.0F, false);
        trim.setTextureOffset(69, 3).addBox(-16.5F, -62.0F, -4.5F, 2.0F, 58.0F, 2.0F, 0.0F, false);
        trim.setTextureOffset(85, 3).addBox(-13.0F, -65.5F, -4.5F, 26.0F, 2.0F, 2.0F, 0.0F, false);
        trim.setTextureOffset(86, 8).addBox(-13.0F, -2.5F, -4.5F, 26.0F, 2.0F, 2.0F, 0.0F, false);

        strap = new ModelRenderer(this);
        strap.setRotationPoint(0.0F, 0.0F, 0.0F);
        box.addChild(strap);
        

        leather = new ModelRenderer(this);
        leather.setRotationPoint(0.0F, 0.0F, 0.0F);
        strap.addChild(leather);
        leather.setTextureOffset(89, 186).addBox(-16.0F, -66.0F, 5.0F, 32.0F, 1.0F, 3.0F, 0.0F, false);
        leather.setTextureOffset(83, 185).addBox(-17.0F, -66.0F, 5.0F, 1.0F, 66.0F, 3.0F, 0.0F, false);
        leather.setTextureOffset(89, 186).addBox(-16.0F, -1.0F, 5.0F, 32.0F, 1.0F, 3.0F, 0.0F, false);
        leather.setTextureOffset(74, 185).addBox(16.0F, -66.0F, 5.0F, 1.0F, 66.0F, 3.0F, 0.0F, false);

        buckle = new ModelRenderer(this);
        buckle.setRotationPoint(0.0F, 0.0F, 0.0F);
        strap.addChild(buckle);
        buckle.setTextureOffset(107, 22).addBox(-17.5F, -56.0F, 5.0F, 2.0F, 1.0F, 4.0F, 0.0F, false);
        buckle.setTextureOffset(107, 22).addBox(-17.5F, -51.0F, 5.0F, 2.0F, 1.0F, 4.0F, 0.0F, false);
        buckle.setTextureOffset(107, 22).addBox(-17.5F, -55.0F, 8.0F, 2.0F, 4.0F, 1.0F, 0.0F, false);
        buckle.setTextureOffset(107, 22).addBox(-17.5F, -56.0F, 4.0F, 2.0F, 6.0F, 1.0F, 0.0F, false);

        corners = new ModelRenderer(this);
        corners.setRotationPoint(0.0F, 0.0F, 0.0F);
        box.addChild(corners);
        

        knobs1 = new ModelRenderer(this);
        knobs1.setRotationPoint(0.0F, 0.0F, 0.0F);
        corners.addChild(knobs1);
        knobs1.setTextureOffset(76, 71).addBox(-17.0F, -4.0F, 13.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs1.setTextureOffset(76, 71).addBox(-16.0F, -3.0F, 15.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs1.setTextureOffset(76, 71).addBox(-17.5F, -3.0F, 14.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs1.setTextureOffset(76, 71).addBox(-16.0F, -1.5F, 14.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        knobs2 = new ModelRenderer(this);
        knobs2.setRotationPoint(0.0F, 0.0F, 0.0F);
        corners.addChild(knobs2);
        knobs2.setTextureOffset(76, 71).addBox(13.0F, -4.0F, 13.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs2.setTextureOffset(76, 71).addBox(14.0F, -3.0F, 15.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs2.setTextureOffset(76, 71).addBox(15.5F, -3.0F, 14.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs2.setTextureOffset(76, 71).addBox(14.0F, -1.5F, 14.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        knobs3 = new ModelRenderer(this);
        knobs3.setRotationPoint(0.0F, 0.0F, 0.0F);
        corners.addChild(knobs3);
        knobs3.setTextureOffset(76, 71).addBox(13.0F, -66.0F, 13.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs3.setTextureOffset(76, 71).addBox(14.0F, -65.0F, 15.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs3.setTextureOffset(76, 71).addBox(15.5F, -65.0F, 14.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs3.setTextureOffset(76, 71).addBox(14.0F, -66.5F, 14.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        knobs4 = new ModelRenderer(this);
        knobs4.setRotationPoint(0.0F, 0.0F, 0.0F);
        corners.addChild(knobs4);
        knobs4.setTextureOffset(76, 71).addBox(-17.0F, -66.0F, 13.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs4.setTextureOffset(76, 71).addBox(-16.0F, -65.0F, 15.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs4.setTextureOffset(76, 71).addBox(-17.5F, -65.0F, 14.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs4.setTextureOffset(76, 71).addBox(-16.0F, -66.5F, 14.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        knobs5 = new ModelRenderer(this);
        knobs5.setRotationPoint(0.0F, 0.0F, 0.0F);
        corners.addChild(knobs5);
        knobs5.setTextureOffset(76, 71).addBox(-17.0F, -66.0F, -4.5F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs5.setTextureOffset(76, 71).addBox(-17.5F, -65.0F, -3.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs5.setTextureOffset(76, 71).addBox(-16.0F, -66.5F, -3.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        knobs6 = new ModelRenderer(this);
        knobs6.setRotationPoint(0.0F, 0.0F, 0.0F);
        corners.addChild(knobs6);
        knobs6.setTextureOffset(76, 71).addBox(13.0F, -66.0F, -4.5F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs6.setTextureOffset(76, 71).addBox(15.5F, -65.0F, -3.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs6.setTextureOffset(76, 71).addBox(14.0F, -66.5F, -3.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        knobs7 = new ModelRenderer(this);
        knobs7.setRotationPoint(0.0F, 0.0F, 0.0F);
        corners.addChild(knobs7);
        knobs7.setTextureOffset(76, 71).addBox(13.0F, -4.0F, -4.5F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs7.setTextureOffset(76, 71).addBox(15.5F, -3.0F, -3.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs7.setTextureOffset(76, 71).addBox(14.0F, -1.5F, -3.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        knobs8 = new ModelRenderer(this);
        knobs8.setRotationPoint(0.0F, 0.0F, 0.0F);
        corners.addChild(knobs8);
        knobs8.setTextureOffset(76, 71).addBox(-17.0F, -4.0F, -4.5F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs8.setTextureOffset(76, 71).addBox(-17.5F, -3.0F, -3.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs8.setTextureOffset(76, 71).addBox(-16.0F, -1.5F, -3.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        hinge = new ModelRenderer(this);
        hinge.setRotationPoint(17.0F, -34.3333F, -4.5F);
        box.addChild(hinge);
        setRotationAngle(hinge, 0.0F, 0.7854F, 0.0F);
        hinge.setTextureOffset(73, 71).addBox(-0.5F, -3.6667F, -0.5F, 1.0F, 6.0F, 1.0F, 0.0F, false);
        hinge.setTextureOffset(73, 71).addBox(-0.5F, -23.6667F, -0.5F, 1.0F, 6.0F, 1.0F, 0.0F, false);
        hinge.setTextureOffset(73, 71).addBox(-0.5F, 20.3333F, -0.5F, 1.0F, 6.0F, 1.0F, 0.0F, false);

        mount = new ModelRenderer(this);
        mount.setRotationPoint(0.0F, 0.0F, 0.0F);
        box.addChild(mount);
        mount.setTextureOffset(80, 71).addBox(16.0F, -12.0F, -4.0F, 1.0F, 2.0F, 4.0F, 0.0F, false);
        mount.setTextureOffset(80, 71).addBox(16.0F, -36.0F, -4.0F, 1.0F, 2.0F, 4.0F, 0.0F, false);
        mount.setTextureOffset(80, 71).addBox(16.0F, -56.0F, -4.0F, 1.0F, 2.0F, 4.0F, 0.0F, false);

        latch = new ModelRenderer(this);
        latch.setRotationPoint(0.0F, 0.0F, 0.0F);
        box.addChild(latch);
        latch.setTextureOffset(61, 75).addBox(-17.0F, -36.0F, -2.75F, 1.0F, 4.0F, 6.0F, 0.0F, false);

        lock = new ModelRenderer(this);
        lock.setRotationPoint(-16.5F, -34.0F, 0.0F);
        box.addChild(lock);
        setRotationAngle(lock, 0.7854F, 0.0F, 0.0F);
        lock.setTextureOffset(65, 78).addBox(-1.0F, -3.5F, -3.5F, 2.0F, 4.0F, 4.0F, 0.0F, false);

        sticker1 = new ModelRenderer(this);
        sticker1.setRotationPoint(-16.0F, -47.0F, 0.0F);
        box.addChild(sticker1);
        setRotationAngle(sticker1, 0.6981F, 0.0F, 0.0F);
        sticker1.setTextureOffset(5, 152).addBox(-0.1F, -3.0F, -2.0F, 1.0F, 6.0F, 8.0F, 0.0F, false);

        sticker2 = new ModelRenderer(this);
        sticker2.setRotationPoint(2.0F, -20.0F, 16.0F);
        box.addChild(sticker2);
        setRotationAngle(sticker2, 0.0F, 0.0F, 0.5236F);
        sticker2.setTextureOffset(4, 154).addBox(2.0F, -4.0F, -0.95F, 12.0F, 8.0F, 1.0F, 0.0F, false);

        lid_rotate_y = new ModelRenderer(this);
        lid_rotate_y.setRotationPoint(17.0F, 24.0F, -4.5F);
        

        walls2 = new ModelRenderer(this);
        walls2.setRotationPoint(-17.0F, 0.0F, 4.5F);
        lid_rotate_y.addChild(walls2);
        

        back_wall2 = new ModelRenderer(this);
        back_wall2.setRotationPoint(0.0F, 0.0F, 0.0F);
        walls2.addChild(back_wall2);
        back_wall2.setTextureOffset(123, 80).addBox(-16.0F, -65.0F, -15.0F, 32.0F, 64.0F, 1.0F, 0.0F, false);
        back_wall2.setTextureOffset(1, 2).addBox(-15.0F, -64.0F, -7.0F, 30.0F, 62.0F, 1.0F, 0.0F, false);

        sidewall_right2 = new ModelRenderer(this);
        sidewall_right2.setRotationPoint(0.0F, 0.0F, 0.0F);
        walls2.addChild(sidewall_right2);
        sidewall_right2.setTextureOffset(124, 78).addBox(15.0F, -64.0F, -14.0F, 1.0F, 62.0F, 9.0F, 0.0F, false);

        sidewall_left2 = new ModelRenderer(this);
        sidewall_left2.setRotationPoint(0.0F, 0.0F, 0.0F);
        walls2.addChild(sidewall_left2);
        sidewall_left2.setTextureOffset(128, 76).addBox(-16.0F, -64.0F, -14.0F, 1.0F, 62.0F, 9.0F, 0.0F, false);

        top_wall2 = new ModelRenderer(this);
        top_wall2.setRotationPoint(0.0F, 0.0F, 0.0F);
        walls2.addChild(top_wall2);
        top_wall2.setTextureOffset(120, 117).addBox(-16.0F, -65.0F, -14.0F, 32.0F, 1.0F, 9.0F, 0.0F, false);

        bottom_wall2 = new ModelRenderer(this);
        bottom_wall2.setRotationPoint(0.0F, 0.0F, 0.0F);
        walls2.addChild(bottom_wall2);
        bottom_wall2.setTextureOffset(152, 117).addBox(-16.0F, -2.0F, -14.0F, 32.0F, 1.0F, 9.0F, 0.0F, false);

        trim2 = new ModelRenderer(this);
        trim2.setRotationPoint(-17.0F, 0.0F, 4.5F);
        lid_rotate_y.addChild(trim2);
        trim2.setTextureOffset(77, 3).addBox(-16.5F, -62.0F, -6.5F, 2.0F, 58.0F, 2.0F, 0.0F, false);
        trim2.setTextureOffset(77, 3).addBox(14.5F, -62.0F, -6.5F, 2.0F, 58.0F, 2.0F, 0.0F, false);
        trim2.setTextureOffset(77, 3).addBox(-15.0F, -65.5F, -15.5F, 30.0F, 2.0F, 2.0F, 0.0F, false);
        trim2.setTextureOffset(77, 3).addBox(-15.0F, -2.5F, -15.5F, 30.0F, 2.0F, 2.0F, 0.0F, false);
        trim2.setTextureOffset(77, 3).addBox(14.5F, -2.5F, -12.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        trim2.setTextureOffset(77, 3).addBox(-16.5F, -2.5F, -12.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        trim2.setTextureOffset(77, 3).addBox(-16.5F, -65.5F, -12.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        trim2.setTextureOffset(77, 3).addBox(14.5F, -65.5F, -12.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
        trim2.setTextureOffset(77, 3).addBox(14.5F, -64.0F, -15.5F, 2.0F, 62.0F, 2.0F, 0.0F, false);
        trim2.setTextureOffset(77, 3).addBox(-16.5F, -64.0F, -15.5F, 2.0F, 62.0F, 2.0F, 0.0F, false);
        trim2.setTextureOffset(77, 3).addBox(-13.0F, -2.5F, -6.5F, 26.0F, 2.0F, 2.0F, 0.0F, false);
        trim2.setTextureOffset(77, 3).addBox(-13.0F, -65.5F, -6.5F, 26.0F, 2.0F, 2.0F, 0.0F, false);

        corners2 = new ModelRenderer(this);
        corners2.setRotationPoint(-17.0F, 0.0F, 4.5F);
        lid_rotate_y.addChild(corners2);
        

        knobs9 = new ModelRenderer(this);
        knobs9.setRotationPoint(0.0F, 0.0F, 0.0F);
        corners2.addChild(knobs9);
        knobs9.setTextureOffset(74, 71).addBox(-17.0F, -4.0F, -16.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs9.setTextureOffset(74, 71).addBox(-16.0F, -3.0F, -16.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs9.setTextureOffset(74, 71).addBox(-17.5F, -3.0F, -15.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs9.setTextureOffset(74, 71).addBox(-16.0F, -1.5F, -15.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        knobs10 = new ModelRenderer(this);
        knobs10.setRotationPoint(0.0F, 0.0F, 0.0F);
        corners2.addChild(knobs10);
        knobs10.setTextureOffset(74, 71).addBox(13.0F, -4.0F, -16.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs10.setTextureOffset(74, 71).addBox(14.0F, -3.0F, -16.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs10.setTextureOffset(74, 71).addBox(15.5F, -3.0F, -15.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs10.setTextureOffset(74, 71).addBox(14.0F, -1.5F, -15.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        knobs11 = new ModelRenderer(this);
        knobs11.setRotationPoint(0.0F, 0.0F, 0.0F);
        corners2.addChild(knobs11);
        knobs11.setTextureOffset(74, 71).addBox(13.0F, -66.0F, -16.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs11.setTextureOffset(74, 71).addBox(14.0F, -65.0F, -16.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs11.setTextureOffset(74, 71).addBox(15.5F, -65.0F, -15.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs11.setTextureOffset(74, 71).addBox(14.0F, -66.5F, -15.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        knobs12 = new ModelRenderer(this);
        knobs12.setRotationPoint(0.0F, 0.0F, 0.0F);
        corners2.addChild(knobs12);
        knobs12.setTextureOffset(74, 71).addBox(-17.0F, -66.0F, -16.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs12.setTextureOffset(74, 71).addBox(-16.0F, -65.0F, -16.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs12.setTextureOffset(74, 71).addBox(-17.5F, -65.0F, -15.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs12.setTextureOffset(74, 71).addBox(-16.0F, -66.5F, -15.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        knobs13 = new ModelRenderer(this);
        knobs13.setRotationPoint(0.0F, 0.0F, 0.0F);
        corners2.addChild(knobs13);
        knobs13.setTextureOffset(74, 71).addBox(-17.0F, -66.0F, -8.5F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs13.setTextureOffset(74, 71).addBox(-17.5F, -65.0F, -7.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs13.setTextureOffset(74, 71).addBox(-16.0F, -66.5F, -7.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        knobs14 = new ModelRenderer(this);
        knobs14.setRotationPoint(0.0F, 0.0F, 0.0F);
        corners2.addChild(knobs14);
        knobs14.setTextureOffset(74, 71).addBox(13.0F, -66.0F, -8.5F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs14.setTextureOffset(74, 71).addBox(15.5F, -65.0F, -7.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs14.setTextureOffset(74, 71).addBox(14.0F, -66.5F, -7.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        knobs15 = new ModelRenderer(this);
        knobs15.setRotationPoint(0.0F, 0.0F, 0.0F);
        corners2.addChild(knobs15);
        knobs15.setTextureOffset(74, 71).addBox(13.0F, -4.0F, -8.5F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs15.setTextureOffset(74, 71).addBox(15.5F, -3.0F, -7.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs15.setTextureOffset(74, 71).addBox(14.0F, -1.5F, -7.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        knobs16 = new ModelRenderer(this);
        knobs16.setRotationPoint(0.0F, 0.0F, 0.0F);
        corners2.addChild(knobs16);
        knobs16.setTextureOffset(74, 71).addBox(-17.0F, -4.0F, -8.5F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        knobs16.setTextureOffset(74, 71).addBox(-17.5F, -3.0F, -7.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        knobs16.setTextureOffset(74, 71).addBox(-16.0F, -1.5F, -7.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        mount2 = new ModelRenderer(this);
        mount2.setRotationPoint(-17.0F, 0.0F, 4.5F);
        lid_rotate_y.addChild(mount2);
        mount2.setTextureOffset(61, 73).addBox(16.0F, -12.0F, -9.0F, 1.0F, 2.0F, 4.0F, 0.0F, false);
        mount2.setTextureOffset(61, 73).addBox(16.0F, -36.0F, -9.0F, 1.0F, 2.0F, 4.0F, 0.0F, false);
        mount2.setTextureOffset(61, 73).addBox(16.0F, -56.0F, -9.0F, 1.0F, 2.0F, 4.0F, 0.0F, false);

        latch2 = new ModelRenderer(this);
        latch2.setRotationPoint(-17.0F, 0.0F, 4.5F);
        lid_rotate_y.addChild(latch2);
        latch2.setTextureOffset(51, 76).addBox(-17.0F, -36.0F, -9.0F, 1.0F, 4.0F, 6.0F, 0.0F, false);

        sticker3 = new ModelRenderer(this);
        sticker3.setRotationPoint(-20.0F, -45.0F, -11.0F);
        lid_rotate_y.addChild(sticker3);
        setRotationAngle(sticker3, 0.0F, 0.0F, -0.6109F);
        sticker3.setTextureOffset(3, 158).addBox(-7.0F, -3.0F, 0.4F, 14.0F, 6.0F, 1.0F, 0.0F, false);

        sticker4 = new ModelRenderer(this);
        sticker4.setRotationPoint(0.0F, 0.0F, -0.5F);
        sticker3.addChild(sticker4);
        setRotationAngle(sticker4, 0.0F, 0.0F, 0.7854F);
        sticker4.setTextureOffset(10, 154).addBox(-4.0F, -4.0F, 0.8F, 8.0F, 8.0F, 1.0F, 0.0F, false);

        boti = new ModelRenderer(this);
        boti.setRotationPoint(0.0F, 24.0F, 0.0F);
        boti.setTextureOffset(4, 181).addBox(-15.0F, -64.0F, -0.25F, 30.0F, 62.0F, 0.0F, 0.0F, false);
        
        textRenderer = new TextLabelModelRenderer(this, TEXT_RENDERER, () -> new String[]{name}, () -> texture);
        textRenderer.setRotationPoint(-6.3F, -2.3F, 0.29F);
        this.sticker3.addChild(textRenderer);
     }
    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    @Override
    public void renderBones(ExteriorTile exterior, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float alpha) {
        this.texture = TrunkExteriorRenderer.TEXTURE;
        name = exterior.getCustomName();
        this.lid_rotate_y.rotateAngleY = (float) Math.toRadians(IDoorType.EnumDoorType.TRUNK.getRotationForState(exterior.getOpen()));
        box.render(matrixStack, buffer, packedLight, packedOverlay, 1, 1, 1, alpha);
        lid_rotate_y.render(matrixStack, buffer, packedLight, packedOverlay, 1, 1, 1, alpha);
    }

    @Override
    public void renderBoti(ExteriorTile exterior, float scale, MatrixStack matrixStack, IVertexBuilder buffer,
            int packedLight, int packedOverlay, float alpha) {
        if(exterior.getBotiWorld() != null && exterior.getOpen() != EnumDoorState.CLOSED) {
            PortalInfo info = new PortalInfo();
            info.setPosition(exterior.getPos());
            info.setWorldShell(exterior.getBotiWorld());
            info.setTranslate(matrix -> {
                matrix.translate(-0.5, 0.25, -0.5);
                ExteriorRenderer.applyTransforms(matrix, exterior);
            });
            info.setTranslatePortal(matrix -> {
                matrix.rotate(Vector3f.ZN.rotationDegrees(180));
                matrix.rotate(Vector3f.YP.rotationDegrees(WorldHelper.getAngleFromFacing(exterior.getBotiWorld().getPortalDirection())));
                matrix.translate(-0.5, -0.5, -0.5);
                
            });
            
            info.setRenderPortal((matrix, buf) -> {
                matrix.push();
                matrix.scale(0.5F, 0.5F, 0.5F);
                this.boti.render(matrix, buf.getBuffer(RenderType.getEntityCutout(texture)), packedLight, packedOverlay);
                matrix.pop();
            });
            
            BOTIRenderer.addPortal(info);
        }
    }

    @Override
    public void renderBrokenExterior(MatrixStack matrix, IVertexBuilder vertex, int light, int overlay) {
        this.texture = TrunkExteriorRenderer.TEXTURE;
        matrix.translate(0, -0.25, 0);
        matrix.scale(0.5F, 0.5F, 0.5F);
        this.box.render(matrix, vertex, light, overlay);
        this.lid_rotate_y.rotateAngleY = 0;
        this.lid_rotate_y.render(matrix, vertex, light, overlay);
    }
}