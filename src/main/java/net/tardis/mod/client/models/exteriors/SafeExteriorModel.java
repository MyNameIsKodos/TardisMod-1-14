package net.tardis.mod.client.models.exteriors;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.client.TRenderTypes;
import net.tardis.mod.client.renderers.boti.BOTIRenderer;
import net.tardis.mod.client.renderers.boti.PortalInfo;
import net.tardis.mod.client.renderers.exteriors.BrokenExteriorRenderer.IBrokenExteriorModel;
import net.tardis.mod.client.renderers.exteriors.ExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.SafeExteriorRenderer;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.enums.EnumMatterState;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class SafeExteriorModel extends ExteriorModel implements IBrokenExteriorModel{
    private final ModelRenderer BOTI;
    private final ModelRenderer door;
    private final ModelRenderer bb_main;

    public SafeExteriorModel() {
        textureWidth = 128;
        textureHeight = 128;

        BOTI = new ModelRenderer(this);
        BOTI.setRotationPoint(0.0F, 24.0F, 0.0F);
        BOTI.setTextureOffset(79, 1).addBox(-8.0F, -34.0F, -6.0F, 16.0F, 34.0F, 1.0F, -0.025F, false);

        door = new ModelRenderer(this);
        door.setRotationPoint(7.0F, 7.0F, -6.5F);
        door.setTextureOffset(30, 48).addBox(-14.0F, -16.0F, -0.5F, 14.0F, 32.0F, 1.0F, 0.0F, false);
        door.setTextureOffset(60, 26).addBox(-1.5F, -14.0F, -1.475F, 2.0F, 6.0F, 2.0F, 0.0F, false);
        door.setTextureOffset(60, 26).addBox(-1.5F, 8.0F, -1.475F, 2.0F, 6.0F, 2.0F, 0.0F, false);
        door.setTextureOffset(60, 34).addBox(-6.0F, -9.0F, -1.0F, 1.0F, 4.0F, 3.0F, 0.0F, false);
        door.setTextureOffset(60, 34).addBox(-6.0F, 5.0F, -1.0F, 1.0F, 4.0F, 3.0F, 0.0F, false);
        door.setTextureOffset(60, 10).addBox(-11.5F, -2.5F, -1.5F, 5.0F, 5.0F, 3.0F, 0.0F, false);
        door.setTextureOffset(60, 60).addBox(-14.0F, -8.5F, -0.475F, 14.0F, 3.0F, 2.0F, 0.0F, false);
        door.setTextureOffset(60, 60).addBox(-14.0F, 5.5F, -0.475F, 14.0F, 3.0F, 2.0F, 0.0F, false);

        bb_main = new ModelRenderer(this);
        bb_main.setRotationPoint(0.0F, 24.0F, 0.0F);
        bb_main.setTextureOffset(0, 0).addBox(-8.0F, -34.0F, -7.0F, 16.0F, 34.0F, 14.0F, 0.0F, false);
        bb_main.setTextureOffset(46, 0).addBox(-7.0F, -10.0F, 7.0F, 14.0F, 9.0F, 1.0F, 0.0F, false);
        bb_main.setTextureOffset(0, 0).addBox(7.0F, -31.0F, -3.0F, 1.0F, 3.0F, 6.0F, 0.0F, false);
        bb_main.setTextureOffset(0, 0).addBox(-8.0F, -31.0F, -3.0F, 1.0F, 3.0F, 6.0F, 0.0F, true);
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    @Override
    public void renderBones(ExteriorTile exterior, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float alpha) {
        matrixStack.push();
        matrixStack.scale(1f, 1f, 1f);
        matrixStack.translate(0, -0.75f, 0);
        this.door.rotateAngleY = (float) Math.toRadians(EnumDoorType.SAFE.getRotationForState(exterior.getOpen()));
        door.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
        //BOTI.render(matrixStack, buffer, packedLight, packedOverlay);
        bb_main.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
        matrixStack.pop();
    }

    @Override
    public void renderBoti(ExteriorTile exterior, float scale, MatrixStack matrixStack, IVertexBuilder buffer,
            int packedLight, int packedOverlay, float alpha) {
        if(exterior.getBotiWorld() != null && exterior.getMatterState() == EnumMatterState.SOLID && exterior.getOpen() != EnumDoorState.CLOSED) {

            PortalInfo info = new PortalInfo();

            info.setPosition(exterior.getPos());
            info.setWorldShell(exterior.getBotiWorld());

            info.setTranslate(matrix -> {
                matrix.translate(-0.5, 0, -0.5);
                ExteriorRenderer.applyTransforms(matrix, exterior);
                matrix.translate(0, -1, 0);
            });

            info.setTranslatePortal(matrix -> {
                matrix.rotate(Vector3f.ZN.rotationDegrees(180));
                matrix.rotate(Vector3f.YP.rotationDegrees(WorldHelper.getAngleFromFacing(exterior.getBotiWorld().getPortalDirection())));
                matrix.translate(-0.5, -1.5, -0.5);
            });

            info.setRenderPortal((matrix, buf) -> {
                matrix.push();
                this.BOTI.render(matrix, buf.getBuffer(TRenderTypes.getTardis(Helper.getVariantTextureOr(exterior.getVariant(), SafeExteriorRenderer.TEXTURE))), packedLight, packedOverlay);
                matrix.pop();
            });

            BOTIRenderer.addPortal(info);

        }
    }

    @Override
    public void renderBrokenExterior(MatrixStack matrix, IVertexBuilder buffer, int packedLight,
            int packedOverlay) {
        matrix.push();
        float scale = 1F;
        matrix.scale(scale, scale, scale);
        matrix.translate(0, -0.75f, 0);
        matrix.translate(0, -0.26, 0);
        
        door.render(matrix, buffer, packedLight, packedOverlay);
        bb_main.render(matrix, buffer, packedLight, packedOverlay);
        matrix.pop();
    }
 
}