package net.tardis.mod.client.models;

import net.minecraft.tileentity.TileEntity;

public interface TileAnimation<T extends TileEntity> {
    public default void animate(T tileEntityIn){}
}
