package net.tardis.mod.client.models.exteriors;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.client.TRenderTypes;
import net.tardis.mod.client.renderers.boti.BOTIRenderer;
import net.tardis.mod.client.renderers.boti.PortalInfo;
import net.tardis.mod.client.renderers.exteriors.ExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.TelephoneExteriorRenderer;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.enums.EnumMatterState;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class TelephoneExteriorModel extends ExteriorModel{
    private final ModelRenderer door;
    private final ModelRenderer BOTI;
    private final ModelRenderer bone2;
    private final ModelRenderer bone;
    private final ModelRenderer bone3;
    private final ModelRenderer bone4;
    private final ModelRenderer bone10;
    private final ModelRenderer bone11;
    private final ModelRenderer bone9;
    private final ModelRenderer bone12;
    private final ModelRenderer bone7;
    private final ModelRenderer bone8;
    private final ModelRenderer bone5;
    private final ModelRenderer bone6;
    private final ModelRenderer bone13;
    private final ModelRenderer bone14;
    private final ModelRenderer bone15;
    private final ModelRenderer bb_main;

    public TelephoneExteriorModel() {
        textureWidth = 256;
        textureHeight = 256;

        door = new ModelRenderer(this);
        door.setRotationPoint(-7.0F, 5.5F, -8.0F);
        door.setTextureOffset(86, 86).addBox(0.0F, -16.5F, 0.0F, 14.0F, 33.0F, 1.0F, 0.0F, false);
        door.setTextureOffset(0, 4).addBox(0.0F, -10.5F, -1.0F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        door.setTextureOffset(0, 4).addBox(0.0F, 6.5F, -1.0F, 1.0F, 4.0F, 1.0F, 0.0F, false);
        door.setTextureOffset(60, 11).addBox(0.0F, 10.5F, -0.5F, 14.0F, 6.0F, 1.0F, 0.0F, false);
        door.setTextureOffset(0, 0).addBox(11.0F, -4.0F, -1.0F, 2.0F, 3.0F, 1.0F, 0.0F, false);

        BOTI = new ModelRenderer(this);
        BOTI.setRotationPoint(0.0F, 24.0F, 0.0F);
        BOTI.setTextureOffset(0, 111).addBox(-7.0F, -37.0F, -6.0F, 14.0F, 35.0F, 1.0F, 0.0F, false);

        bone2 = new ModelRenderer(this);
        bone2.setRotationPoint(0.0F, 24.0F, 0.0F);
        bone2.setTextureOffset(30, 72).addBox(7.0F, -37.0F, -8.0F, 2.0F, 35.0F, 1.0F, 0.0F, false);
        bone2.setTextureOffset(42, 72).addBox(7.0F, -37.0F, -9.0F, 1.0F, 35.0F, 1.0F, 0.0F, false);

        bone = new ModelRenderer(this);
        bone.setRotationPoint(8.0F, -20.0F, -9.0F);
        bone2.addChild(bone);
        setRotationAngle(bone, 0.0F, -0.7854F, 0.0F);
        bone.setTextureOffset(46, 72).addBox(0.0F, -17.0F, 0.0F, 1.0F, 35.0F, 1.0F, 0.0F, false);
        bone.setTextureOffset(46, 72).addBox(0.4142F, -17.0F, 0.0F, 1.0F, 35.0F, 1.0F, 0.0F, false);

        bone3 = new ModelRenderer(this);
        bone3.setRotationPoint(0.0F, 24.0F, 0.0F);
        bone3.setTextureOffset(36, 72).addBox(-9.0F, -37.0F, -8.0F, 2.0F, 35.0F, 1.0F, 0.0F, false);
        bone3.setTextureOffset(50, 72).addBox(-8.0F, -37.0F, -9.0F, 1.0F, 35.0F, 1.0F, 0.0F, false);

        bone4 = new ModelRenderer(this);
        bone4.setRotationPoint(-8.0F, -20.0F, -9.0F);
        bone3.addChild(bone4);
        setRotationAngle(bone4, 0.0F, 0.7854F, 0.0F);
        bone4.setTextureOffset(54, 99).addBox(-1.0F, -17.0F, 0.0F, 1.0F, 35.0F, 1.0F, 0.0F, false);
        bone4.setTextureOffset(54, 99).addBox(-1.4142F, -17.0F, 0.0F, 1.0F, 35.0F, 1.0F, 0.0F, false);

        bone10 = new ModelRenderer(this);
        bone10.setRotationPoint(-8.0F, -17.0F, 0.0F);
        setRotationAngle(bone10, 0.0F, 0.0F, -0.3752F);
        bone10.setTextureOffset(48, 24).addBox(-1.0F, -1.0F, -9.0F, 10.0F, 1.0F, 18.0F, 0.0F, false);

        bone11 = new ModelRenderer(this);
        bone11.setRotationPoint(-1.0F, -17.0F, -8.0F);
        setRotationAngle(bone11, 0.3752F, 0.0F, 0.0F);
        bone11.setTextureOffset(0, 61).addBox(-8.0F, -1.0F, -1.0F, 18.0F, 1.0F, 10.0F, 0.0F, false);

        bone9 = new ModelRenderer(this);
        bone9.setRotationPoint(8.0F, -17.0F, 0.0F);
        setRotationAngle(bone9, 0.0F, 0.0F, 0.3752F);
        bone9.setTextureOffset(46, 46).addBox(-9.0F, -1.0F, -9.0F, 10.0F, 1.0F, 18.0F, 0.0F, false);

        bone12 = new ModelRenderer(this);
        bone12.setRotationPoint(-1.0F, -17.0F, 8.0F);
        setRotationAngle(bone12, -0.3752F, 0.0F, 0.0F);
        bone12.setTextureOffset(60, 0).addBox(-8.0F, -1.0F, -9.0F, 18.0F, 1.0F, 10.0F, 0.0F, false);

        bone7 = new ModelRenderer(this);
        bone7.setRotationPoint(0.0F, 24.0F, 0.0F);
        bone7.setTextureOffset(36, 72).addBox(-9.0F, -37.0F, 7.0F, 2.0F, 35.0F, 1.0F, 0.0F, false);
        bone7.setTextureOffset(50, 72).addBox(-8.0F, -37.0F, 8.0F, 1.0F, 35.0F, 1.0F, 0.0F, false);

        bone8 = new ModelRenderer(this);
        bone8.setRotationPoint(-8.0F, -20.0F, 9.0F);
        bone7.addChild(bone8);
        setRotationAngle(bone8, 0.0F, -0.7854F, 0.0F);
        bone8.setTextureOffset(54, 99).addBox(-1.0F, -17.0F, -1.0F, 1.0F, 35.0F, 1.0F, 0.0F, false);
        bone8.setTextureOffset(54, 99).addBox(-1.4142F, -17.0F, -1.0F, 1.0F, 35.0F, 1.0F, 0.0F, false);

        bone5 = new ModelRenderer(this);
        bone5.setRotationPoint(0.0F, 24.0F, 0.0F);
        bone5.setTextureOffset(30, 72).addBox(7.0F, -37.0F, 7.0F, 2.0F, 35.0F, 1.0F, 0.0F, false);
        bone5.setTextureOffset(42, 72).addBox(7.0F, -37.0F, 8.0F, 1.0F, 35.0F, 1.0F, 0.0F, false);

        bone6 = new ModelRenderer(this);
        bone6.setRotationPoint(8.0F, -20.0F, 9.0F);
        bone5.addChild(bone6);
        setRotationAngle(bone6, 0.0F, 0.7854F, 0.0F);
        bone6.setTextureOffset(46, 72).addBox(0.0F, -17.0F, -1.0F, 1.0F, 35.0F, 1.0F, 0.0F, false);
        bone6.setTextureOffset(46, 72).addBox(0.4142F, -17.0F, -1.0F, 1.0F, 35.0F, 1.0F, 0.0F, false);

        bone13 = new ModelRenderer(this);
        bone13.setRotationPoint(0.0F, 55.0F, 0.0F);
        setRotationAngle(bone13, 0.0F, -1.5708F, 0.0F);
        bone13.setTextureOffset(0, 72).addBox(-7.0F, -66.0F, -8.025F, 14.0F, 33.0F, 1.0F, 0.0F, false);
        bone13.setTextureOffset(60, 11).addBox(-7.0F, -39.0F, -8.525F, 14.0F, 6.0F, 1.0F, 0.0F, false);

        bone14 = new ModelRenderer(this);
        bone14.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone13.addChild(bone14);
        setRotationAngle(bone14, 0.0F, -1.5708F, 0.0F);
        bone14.setTextureOffset(0, 72).addBox(-7.0F, -66.0F, -8.0F, 14.0F, 33.0F, 1.0F, 0.0F, false);
        bone14.setTextureOffset(60, 11).addBox(-7.0F, -39.0F, -8.5F, 14.0F, 6.0F, 1.0F, 0.0F, false);

        bone15 = new ModelRenderer(this);
        bone15.setRotationPoint(0.0F, 0.0F, 0.0F);
        bone14.addChild(bone15);
        setRotationAngle(bone15, 0.0F, -1.5708F, 0.0F);
        bone15.setTextureOffset(0, 72).addBox(-7.0F, -66.0F, -8.025F, 14.0F, 33.0F, 1.0F, 0.0F, false);
        bone15.setTextureOffset(60, 11).addBox(-7.0F, -39.0F, -8.525F, 14.0F, 6.0F, 1.0F, 0.0F, false);

        bb_main = new ModelRenderer(this);
        bb_main.setRotationPoint(0.0F, 24.0F, 0.0F);
        bb_main.setTextureOffset(0, 0).addBox(-10.0F, -2.0F, -10.0F, 20.0F, 2.0F, 20.0F, 0.0F, false);
        bb_main.setTextureOffset(48, 43).addBox(-7.0F, -37.0F, -9.0F, 14.0F, 2.0F, 1.0F, 0.0F, false);
        bb_main.setTextureOffset(0, 22).addBox(-8.0F, -41.0F, -8.0F, 16.0F, 4.0F, 16.0F, 0.0F, false);
        bb_main.setTextureOffset(84, 59).addBox(-6.0F, -40.25F, -8.5F, 12.0F, 3.0F, 1.0F, 0.0F, false);
        bb_main.setTextureOffset(86, 11).addBox(-8.5F, -40.25F, -6.0F, 1.0F, 3.0F, 12.0F, 0.0F, false);
        bb_main.setTextureOffset(84, 59).addBox(-6.0F, -40.25F, 7.5F, 12.0F, 3.0F, 1.0F, 0.0F, false);
        bb_main.setTextureOffset(86, 11).addBox(7.5F, -40.25F, -6.0F, 1.0F, 3.0F, 12.0F, 0.0F, false);
        bb_main.setTextureOffset(0, 42).addBox(-8.0F, -44.0F, -8.0F, 16.0F, 3.0F, 16.0F, 0.0F, false);
        bb_main.setTextureOffset(84, 43).addBox(-9.0F, -37.0F, -7.0F, 1.0F, 2.0F, 14.0F, 0.0F, false);
        bb_main.setTextureOffset(84, 43).addBox(8.0F, -37.0F, -7.0F, 1.0F, 2.0F, 14.0F, 0.0F, false);
        bb_main.setTextureOffset(48, 43).addBox(-7.0F, -37.0F, 8.0F, 14.0F, 2.0F, 1.0F, 0.0F, false);
    }
    
    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    @Override
    public void renderBones(ExteriorTile exterior, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float alpha) {
        
        this.door.rotateAngleY = (float) Math.toRadians(EnumDoorType.TELEPHONE.getRotationForState(exterior.getOpen()));

        door.render(matrixStack, buffer, packedLight, packedOverlay, 1, 1, 1, alpha);
        //BOTI.render(matrixStack, buffer, packedLight, packedOverlay);
        bone2.render(matrixStack, buffer, packedLight, packedOverlay, 1, 1, 1, alpha);
        bone3.render(matrixStack, buffer, packedLight, packedOverlay, 1, 1, 1, alpha);
        bone10.render(matrixStack, buffer, packedLight, packedOverlay, 1, 1, 1, alpha);
        bone11.render(matrixStack, buffer, packedLight, packedOverlay, 1, 1, 1, alpha);
        bone9.render(matrixStack, buffer, packedLight, packedOverlay, 1, 1, 1, alpha);
        bone12.render(matrixStack, buffer, packedLight, packedOverlay, 1, 1, 1, alpha);
        bone7.render(matrixStack, buffer, packedLight, packedOverlay, 1, 1, 1, alpha);
        bone5.render(matrixStack, buffer, packedLight, packedOverlay, 1, 1, 1, alpha);
        bone13.render(matrixStack, buffer, packedLight, packedOverlay, 1, 1, 1, alpha);
        bb_main.render(matrixStack, buffer, packedLight, packedOverlay, 1, 1, 1, alpha);
    }

    @Override
    public void renderBoti(ExteriorTile exterior, float scale, MatrixStack matrixStack, IVertexBuilder buffer,
            int packedLight, int packedOverlay, float alpha) {
        if(exterior.getBotiWorld() != null && exterior.getMatterState() == EnumMatterState.SOLID && exterior.getOpen() != EnumDoorState.CLOSED) {

            PortalInfo info = new PortalInfo();

            info.setPosition(exterior.getPos());
            info.setWorldShell(exterior.getBotiWorld());

            info.setTranslate(matrix -> {
                matrix.translate(-0.5, 0, -0.5);
                ExteriorRenderer.applyTransforms(matrix, exterior);
                matrix.translate(0, -0.95, 0);
            });

            info.setTranslatePortal(matrix -> {
                matrix.rotate(Vector3f.ZN.rotationDegrees(180));
                matrix.rotate(Vector3f.YP.rotationDegrees(WorldHelper.getAngleFromFacing(exterior.getBotiWorld().getPortalDirection())));
                matrix.translate(-0.5, -1.5, -0.5);
            });

            info.setRenderPortal((matrix, buf) -> {
                matrix.push();
                this.BOTI.render(matrix, buf.getBuffer(TRenderTypes.getTardis(Helper.getVariantTextureOr(exterior.getVariant(), TelephoneExteriorRenderer.TEXTURE))), packedLight, packedOverlay);
                matrix.pop();
            });

            BOTIRenderer.addPortal(info);

        }
    }
}