package net.tardis.mod.client.renderers.boti;

import java.util.SortedMap;

import it.unimi.dsi.fastutil.objects.Object2ObjectLinkedOpenHashMap;
import net.minecraft.client.renderer.Atlases;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RegionRenderCacheBuilder;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ModelBakery;
import net.minecraft.inventory.container.PlayerContainer;
import net.minecraft.util.Util;
import net.tardis.mod.client.TRenderTypes;
/**
 * <p> Cached BufferBuilder instances and its preconstructed IRenderTypeBuffer#Impl
 * <br> This allows us to render Tile entities without trying to violate their buffer builder states
 * Based on RenderTypeBuffers, but includes our Tardis Render Types*/
public class BotiTileBuffers {

    private final RegionRenderCacheBuilder fixedBuilder = new RegionRenderCacheBuilder();
    public SortedMap<RenderType, BufferBuilder> buffers = Util.make(new Object2ObjectLinkedOpenHashMap<>(), (map) -> {
    	map.put(Atlases.getSolidBlockType(), this.fixedBuilder.getBuilder(RenderType.getSolid()));
        map.put(Atlases.getCutoutBlockType(), this.fixedBuilder.getBuilder(RenderType.getCutout()));
        map.put(Atlases.getBannerType(), this.fixedBuilder.getBuilder(RenderType.getCutoutMipped()));
        map.put(Atlases.getTranslucentCullBlockType(), this.fixedBuilder.getBuilder(RenderType.getTranslucent()));
        put(map, TRenderTypes.getTardis(PlayerContainer.LOCATION_BLOCKS_TEXTURE));
        put(map, Atlases.getShieldType());
        put(map, Atlases.getBedType());
        put(map, Atlases.getShulkerBoxType());
        put(map, Atlases.getSignType());
        put(map, Atlases.getChestType());
        put(map, RenderType.getTranslucentNoCrumbling());
        put(map, RenderType.getArmorGlint());
        put(map, RenderType.getArmorEntityGlint());
        put(map, RenderType.getGlint());
        put(map, RenderType.getGlintDirect());
        put(map, RenderType.getGlintTranslucent());
        put(map, RenderType.getEntityGlint());
        put(map, RenderType.getEntityGlintDirect());
        put(map, RenderType.getWaterMask());
        ModelBakery.DESTROY_RENDER_TYPES.forEach((renderType) -> {
           put(map, renderType);
        });
    });
    
    private final IRenderTypeBuffer.Impl bufferSource = IRenderTypeBuffer.getImpl(this.buffers, new BufferBuilder(256));
    
    private static void put(Object2ObjectLinkedOpenHashMap<RenderType, BufferBuilder> mapBuildersIn, RenderType renderTypeIn) {
        mapBuildersIn.put(renderTypeIn, new BufferBuilder(renderTypeIn.getBufferSize()));
     }

    public IRenderTypeBuffer.Impl getBufferSource() {
        return this.bufferSource;
    }


}
