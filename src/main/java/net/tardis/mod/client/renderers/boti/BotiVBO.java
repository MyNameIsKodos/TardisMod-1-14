package net.tardis.mod.client.renderers.boti;

import java.util.Map;

import org.lwjgl.opengl.GL11;

import com.google.common.collect.Maps;

import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexBuffer;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.util.math.vector.Matrix4f;

public class BotiVBO {

    public VertexFormat format = DefaultVertexFormats.BLOCK;
    public Map<RenderType, BufferBuilder> buffers = Maps.newHashMap();
    public Map<RenderType, VertexBuffer> vbos = Maps.newHashMap();

    public BotiVBO(){
        init();
    }

    public void init(){
        for(RenderType type : RenderType.getBlockRenderTypes()){
            buffers.put(type, new BufferBuilder(format.getSize()));
            vbos.put(type, new VertexBuffer(format));
        }
    }

    public void begin(RenderType type){
        this.getVBO(type).bindBuffer();
    }

    public VertexBuffer getVBO(RenderType type){
        return this.vbos.getOrDefault(type, this.vbos.get(RenderType.getSolid()));
    }

    public BufferBuilder getBufferBuilder(RenderType type){
        return this.buffers.getOrDefault(type, this.buffers.get(RenderType.getSolid()));
    }

    public void resetData(RenderType type){
        this.getBufferBuilder(type).discard();
        this.getBufferBuilder(type).reset();
    }

    public void upload(RenderType type){
        BufferBuilder bb = this.getBufferBuilder(type);
        if(bb.isDrawing()) {
            bb.finishDrawing();
//            System.out.println("Stopped Drawing");
        }
        this.getVBO(type).upload(bb);
//        System.out.println("Uploaded to VBO");
    }

    public void unbind(RenderType type){
        this.getBufferBuilder(type).reset();
        this.getBufferBuilder(type).discard();
        VertexBuffer.unbindBuffer();
    }

    public void draw(Matrix4f matrix){
        for(RenderType type : this.vbos.keySet()){
            this.begin(type);
            format.setupBufferState(0L);
            type.setupRenderState();
            this.getVBO(type).draw(matrix, GL11.GL_QUADS);
            type.clearRenderState();
            format.clearBufferState();
            VertexBuffer.unbindBuffer();
        }

    }


}
