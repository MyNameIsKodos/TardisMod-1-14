package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.tardis.mod.tileentities.machines.ArtronCollectorTile;

public class ArtronCollectorRenderer extends TileEntityRenderer<ArtronCollectorTile> {

    public ArtronCollectorRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
        super(rendererDispatcherIn);
    }

    @Override
    public void render(ArtronCollectorTile tile, float partialTicks, MatrixStack matrixStackIn,
            IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
        matrixStackIn.push();
        double flo = Math.cos(Minecraft.getInstance().world.getGameTime() * 0.05) * 0.05;
        matrixStackIn.translate(0.25, 0.6 + flo, 0.25);
        matrixStackIn.translate(0.25, 0.75 + flo, 0.25);
        //Use ground transform here. No need to scale, the reason why some of our items are big is because the item json made the ground display scale too large
        Minecraft.getInstance().getItemRenderer().renderItem(tile.getItem(), TransformType.GROUND, combinedLightIn, combinedOverlayIn, matrixStackIn, bufferIn);
        matrixStackIn.pop();
    }

}
