package net.tardis.mod.traits;

import net.minecraft.block.BlockState;
import net.minecraft.block.FireBlock;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.math.BlockPos;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.EmotionHandler.EnumHappyState;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class PyroTrait extends TardisTrait{

	public PyroTrait(TardisTraitType type) {
		super(type);
	}

	@Override
	public void tick(ConsoleTile tile) {
		if(tile.getWorld().getGameTime() % 400 == 0 && tile.getEmotionHandler().getMood() < EnumHappyState.ECSTATIC.getTreshold()) {
			int radius = 16;
			
			ExteriorTile ext = tile.getExteriorType().getExteriorTile(tile);
			if(ext != null) {
				BlockPos.Mutable start = new BlockPos.Mutable();
				start.setPos(ext.getPos());
				for(int x = -radius; x < radius; ++x) {
					for(int y = -radius; y < radius; ++y) {
						for(int z = -radius; z < radius; ++z) {
							BlockState state = ext.getWorld().getBlockState(start.add(x, y, z));
							if(state.getFluidState().isTagged(FluidTags.LAVA) || state.getBlock() instanceof FireBlock) {
								tile.getEmotionHandler().addMood(5 + (this.getModifier() * 30));
								this.warnPlayer(tile, LIKES_LOCATION);
								return;
							}
						}
					}
				}
			}
		}
	}

}
