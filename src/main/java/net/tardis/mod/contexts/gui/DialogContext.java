package net.tardis.mod.contexts.gui;

import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.missions.misc.Dialog;

public class DialogContext extends GuiContext{

	public Dialog dialog;
	
	public DialogContext(Dialog dia) {
		this.dialog = dia;
	} 
}
