package net.tardis.mod.compat.jei;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.Lists;
import com.mojang.blaze3d.matrix.MatrixStack;

import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.drawable.IDrawableAnimated;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.client.gui.GuiUtils;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.recipe.AlembicRecipe;
import net.tardis.mod.recipe.AlembicRecipe.ResultType;
import net.tardis.mod.tileentities.machines.AlembicTile;

public class AlembicRecipeCategory implements IRecipeCategory<AlembicRecipe> {
    public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/containers/alembic.png");
    public static final ResourceLocation NAME = new ResourceLocation(Tardis.MODID, "alembic");
    private IDrawable background;
    private IDrawable icon;
    private IDrawable arrow;
    
    public AlembicRecipeCategory(IGuiHelper help){
        background = help.createDrawable(TEXTURE,
                4, 3, 169, 67);
        icon = help.createDrawableIngredient(new ItemStack(TBlocks.alembic.get()));
        arrow = help.drawableBuilder(TEXTURE, 178, 1, 21, 14)
                .buildAnimated(200, IDrawableAnimated.StartDirection.LEFT, false);
        
    }
    
    @Override
    public ResourceLocation getUid() {
        return NAME;
    }

    @Override
    public Class<AlembicRecipe> getRecipeClass() {
        return AlembicRecipe.class;
    }

    @Override
    public String getTitle() {
        return "Alembic";
    }

    @Override
    public IDrawable getBackground() {
        return this.background;
    }

    @Override
    public IDrawable getIcon() {
        return this.icon;
    }

    @Override
    public void setIngredients(AlembicRecipe recipe, IIngredients ingre) {
        ingre.setInputIngredients(Lists.newArrayList(recipe.getRequiredBurnableIngredient()));
        if(!recipe.getResult().isEmpty())
            ingre.setOutput(VanillaTypes.ITEM, recipe.getResult());
        else ingre.setOutput(VanillaTypes.ITEM, new ItemStack(TItems.MERCURY_BOTTLE.get()));
    }

    @Override
    public void setRecipe(IRecipeLayout recipeLayout, AlembicRecipe recipe, IIngredients ingre) {
        if (recipe.getResultType() != ResultType.FLUID_COLLECTION) {
            recipeLayout.getItemStacks().init(0, false, 20, 7);
            recipeLayout.getItemStacks().set(0, recipe.getRequiredWater() != 0 ? new ItemStack(Items.WATER_BUCKET) : new ItemStack(Items.AIR));
            
            recipeLayout.getItemStacks().init(1, false, 20, 43);
            recipeLayout.getItemStacks().set(1, recipe.getRequiredWater() != 0 ? new ItemStack(Items.BUCKET) : new ItemStack(Items.AIR));
            
            recipeLayout.getItemStacks().init(2, true, 57, 7);
            List<ItemStack> resultStacks = Lists.newArrayList(recipe.getRequiredBurnableIngredient().getMatchingStacks());
            if (!resultStacks.isEmpty()) {
                List<ItemStack> displayStacks = new ArrayList<>();
                for (ItemStack stack : resultStacks) {
                    stack.setCount(recipe.getRequiredIngredientCount());
                    displayStacks.add(stack);
                }

                recipeLayout.getItemStacks().set(2, displayStacks);
            }
            List<ItemStack> burnables = Lists.newArrayList(Ingredient.fromTag(ItemTags.COALS).getMatchingStacks());
            recipeLayout.getItemStacks().init(3, false, 57, 43);
            recipeLayout.getItemStacks().set(3, burnables);
            if (!recipe.getResult().isEmpty()){
                recipeLayout.getItemStacks().init(4, false, 135, 43);
                recipeLayout.getItemStacks().set(4, recipe.getResult());
            }
        }
        else {
            recipeLayout.getItemStacks().init(0, false, 135, 7);
            
            List<ItemStack> fluidCollectionStacks = Lists.newArrayList(recipe.getFluidCollectionIngredient().getMatchingStacks());
            List<ItemStack> displayFluidCollectionStacks = new ArrayList<>();
            for (ItemStack stack : fluidCollectionStacks) {
                stack.setCount(recipe.getRequiredIngredientCount());
                displayFluidCollectionStacks.add(stack);
            }
            recipeLayout.getItemStacks().set(0, displayFluidCollectionStacks);
            
            recipeLayout.getItemStacks().init(1, false, 135, 43);
            recipeLayout.getItemStacks().set(1, recipe.getResult().isEmpty() ? new ItemStack(TItems.MERCURY_BOTTLE.get()) : recipe.getResult());
        }
        
    }

    @Override
    public void draw(AlembicRecipe recipe, MatrixStack matrixStack, double mouseX, double mouseY) {
        IRecipeCategory.super.draw(recipe, matrixStack, mouseX, mouseY);
        arrow.draw(matrixStack, 86, 28);
        int mouseXPos = (int)mouseX;
        int mouseYPos = (int)mouseY;
        Minecraft mc = Minecraft.getInstance();
        
        TranslationTextComponent waterName = new TranslationTextComponent(Fluids.WATER.getAttributes().getTranslationKey());
        
        int requiredNonWaterFluid = recipe.getResultType() == ResultType.FLUID_COLLECTION ? recipe.getRequiredNonWaterFluidAmount() : recipe.getGeneratedFluid();
        List<ITextComponent> nonWaterFluidTankText = TextHelper.createSpecialCaseFluidTankToolTip(requiredNonWaterFluid, AlembicTile.MAX_NON_WATER, TardisConstants.ContainerComponentTranslations.MERCURY);
        
        List<ITextComponent> waterTankText = TextHelper.createSpecialCaseFluidTankToolTip(recipe.getRequiredWater(), AlembicTile.MAX_WATER, waterName);
        List<ITextComponent> progressText = TextHelper.createProgressBarTooltip(0, recipe.getRequiredProgressTicks(), true);
        //Water tank tooltip
        if (Helper.isInBounds(mouseXPos, mouseYPos, 40, 7, 50, 60))
            GuiUtils.drawHoveringText(matrixStack, waterTankText, mouseXPos, mouseYPos, 150, 50, 100, mc.fontRenderer);
        
        //Fluid Tank tooltip
        if (Helper.isInBounds(mouseXPos, mouseYPos, 115, 7, 125, 60))
            GuiUtils.drawHoveringText(matrixStack, nonWaterFluidTankText, mouseXPos, mouseYPos, 100, 50, 100, mc.fontRenderer);
        
        //Progress bar tooltip
        if (Helper.isInBounds(mouseXPos, mouseYPos, 85, 20, 110, 40))
            GuiUtils.drawHoveringText(matrixStack, progressText, mouseXPos, mouseYPos, 100, 50, 100, mc.fontRenderer);
        
        mc.textureManager.bindTexture(TEXTURE);
        
        //Water tank
        int waterScaled = (int) (53 * ( 1 - (recipe.getRequiredWater() / (double) AlembicTile.MAX_WATER)));
        int offsetWaterBlit = 53 - (53 - waterScaled);
        Screen.blit(matrixStack, 42, 7 + offsetWaterBlit, 193, 19 + offsetWaterBlit, 10, 53 - offsetWaterBlit, 256, 256);
        
        //Water lines
        Screen.blit(matrixStack, 45, 9, 182, 21, 7, 49, 256, 256);
        
        //Non Water Fluid Tank
        int nonWaterScaled = (int) (53 * (1 - (requiredNonWaterFluid / (double) AlembicTile.MAX_NON_WATER)));
        int offsetNonWaterBlit = 53 - (53 - nonWaterScaled);
        Screen.blit(matrixStack, 115, 7 + offsetNonWaterBlit, 207, 19 + offsetNonWaterBlit, 10, 53 - offsetNonWaterBlit, 256, 256);
        
        //Non Water tank lines
        Screen.blit(matrixStack, 118, 9, 182, 21, 7, 49, 256, 256);
    }
}
