package net.tardis.mod.cap.items.sonic;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.items.sonicparts.SonicBasePart;
import net.tardis.mod.registries.SonicModeRegistry;
import net.tardis.mod.schematics.Schematic;
import net.tardis.mod.sonic.AbstractSonicMode;
import net.tardis.mod.sonic.ISonicPart;

import java.util.List;

/**
 * Created by Swirtzly
 * on 22/03/2020 @ 22:32
 */
public interface ISonic extends INBTSerializable<CompoundNBT> {
    void setSonicPart(SonicBasePart.SonicComponentTypes type, ISonicPart.SonicPart part);

    SonicBasePart.SonicComponentTypes getSonicPart(ISonicPart.SonicPart part);

    float getCharge();

    void setCharge(float charge);

    float getMaxCharge();

    float progress();

    void setProgress(float progress);

    void sync(PlayerEntity player, Hand hand);

    void setMode(AbstractSonicMode mode);
    void setMode(ResourceLocation location);

    AbstractSonicMode getMode();

    void randomiseParts();

    int getForgeEnergy();

    void setForgeEnergy(int energy);
    
    ResourceLocation getTardis();
	void setTardis(ResourceLocation tardis);

    List<Schematic> getSchematics();
    void addSchematic(Schematic sce);
}