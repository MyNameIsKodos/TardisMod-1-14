package net.tardis.mod.commands.argument;

import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.arguments.ArgumentType;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.DynamicCommandExceptionType;
import com.mojang.brigadier.suggestion.Suggestions;
import com.mojang.brigadier.suggestion.SuggestionsBuilder;

import net.minecraft.command.CommandSource;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.misc.Disguise;
import net.tardis.mod.registries.DisguiseRegistry;

public class DisguiseArgument implements ArgumentType<ResourceLocation>{
	private static final Collection<String> EXAMPLES = Stream.of(DisguiseRegistry.OAK_TREE.get()).map((disguise) -> {
	      return disguise != null ? disguise.getRegistryName().toString() : ""; //This could be null when the world is being loaded, as the resource manager is reloaded
	   }).collect(Collectors.toList());
    private static final DynamicCommandExceptionType INVALID_DISGUISE_EXCEPTION = new DynamicCommandExceptionType((disguise) -> {
      return new TranslationTextComponent("argument.tardis.disguise.invalid", disguise);
    });
	
	@Override
	public ResourceLocation parse(StringReader reader) throws CommandSyntaxException {
		return ResourceLocation.read(reader);
	}
	
	@Override
	public <S> CompletableFuture<Suggestions> listSuggestions(CommandContext<S> context, SuggestionsBuilder builder) {
		Collection<ResourceLocation> disguises = DisguiseRegistry.DISGUISE_REGISTRY.get().getKeys();
		return context.getSource() instanceof ISuggestionProvider ? ISuggestionProvider.func_212476_a(disguises.stream(), builder) : Suggestions.empty();
	}

	@Override
	public Collection<String> getExamples() {
		return EXAMPLES;
	}

	public static DisguiseArgument getDisguiseArgument() {
	      return new DisguiseArgument();
	}
	
	public static Disguise getDisguise(CommandContext<CommandSource> context, String name) throws CommandSyntaxException {
		ResourceLocation resourcelocation = context.getArgument(name, ResourceLocation.class);
		Disguise mission = DisguiseRegistry.DISGUISE_REGISTRY.get().getValue(resourcelocation);
		if (mission == null)
			throw INVALID_DISGUISE_EXCEPTION.create(resourcelocation);
		else
			return mission;
	}

}
