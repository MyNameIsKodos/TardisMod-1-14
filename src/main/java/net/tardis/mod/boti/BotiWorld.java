package net.tardis.mod.boti;

import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.network.play.ClientPlayNetHandler;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.Entity;
import net.minecraft.fluid.FluidState;
import net.minecraft.profiler.IProfiler;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.DimensionType;
import net.minecraft.world.LightType;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.tardis.mod.boti.stores.BlockStore;
import net.tardis.mod.helper.Helper;

import java.util.function.Supplier;

@OnlyIn(Dist.CLIENT)
public class BotiWorld extends ClientWorld {

	public static Supplier<IProfiler> PROFILER = BotiWorldProfiler::new;
	private static RegistryKey<World> DUMMY_KEY = RegistryKey.getOrCreateKey(Registry.WORLD_KEY, Helper.createRL("boti"));
    private WorldShell shell;
    
    public BotiWorld(ClientPlayNetHandler handler, ClientWorldInfo info,
	        DimensionType dimType, int viewDistance, Supplier<IProfiler> profiler, WorldRenderer worldRenderer,
	        boolean isDebug, long seed) {
		super(Minecraft.getInstance().player.connection, Minecraft.getInstance().world.getWorldInfo(), DUMMY_KEY, dimType, 10, BotiWorld.PROFILER, worldRenderer, false, seed);
		this.calculateInitialSkylight();
		
	}
    
    public BotiWorld(RegistryKey<World> world, WorldShell shell, DimensionType dimType, WorldRenderer worldRenderer, long seed) {
		super(Minecraft.getInstance().player.connection, Minecraft.getInstance().world.getWorldInfo(), world, dimType, 10, BotiWorld.PROFILER, worldRenderer, false, seed);
		this.shell = shell;
		this.shell.setDimensionType(Minecraft.getInstance().world.func_241828_r().getRegistry(Registry.DIMENSION_TYPE_KEY).getOptionalKey(dimType).get());
	}

    @Override
    public Iterable<Entity> getAllEntities() {
        return shell.getEntities();
    }

    @Override
    public Entity getEntityByID(int id) {
        return null;
    }

    @Override
    public BlockState getBlockState(BlockPos pos) {
        return shell.getBlockState(pos);
    }

    @Override
    public TileEntity getTileEntity(BlockPos pos) {
        return shell.getTileEntity(pos);
    }

    @Override
    public FluidState getFluidState(BlockPos pos) {
        return shell.getFluidState(pos);
    }

    @Override
	public Biome getBiome(BlockPos pos) {

    	return shell.getBiome(Minecraft.getInstance().world);
	}
    
    

    @Override
	public int getLight(BlockPos pos) {
    	if(shell == null)
            return 0;

        BlockStore store = shell.get(pos);

       if(store != null){
           return LightTexture.packLight(
					shell.getWorld().getLightFor(LightType.BLOCK, pos),
					shell.getWorld().getLightFor(LightType.SKY, pos)
					);
       }
       return 15;
	}

	@Override
    public int getLightFor(LightType lightTypeIn, BlockPos blockPosIn) {

        if(shell == null)
            return 0;

        BlockStore store = shell.get(blockPosIn);

        if(store != null){
            if(lightTypeIn == LightType.SKY)
                return store.getSkyLight();
            return store.getLight();
        }
        return lightTypeIn == LightType.SKY ? 15 : 0;
    }

    @Override
    public void tickBlockEntities() {
        if (this.shell != null) {
            for (TileEntity te : this.shell.getTiles().values()) {
                if (te instanceof ITickableTileEntity)
                    ((ITickableTileEntity) te).tick();
            }
        }
    }

    public void setShell(WorldShell shell) {
        this.shell = shell;
        //this.calculateInitialSkylight();
    }

	public static BotiWorld copy(ClientWorld world, DimensionType type, WorldRenderer renderer) {
		return new BotiWorld(
				Minecraft.getInstance().getConnection(),
				world.getWorldInfo(),
				type,
				12,
				BotiWorld.PROFILER,
                renderer,
				true,
				1234
		);
	}

}
