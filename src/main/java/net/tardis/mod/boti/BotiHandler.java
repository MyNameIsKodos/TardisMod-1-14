package net.tardis.mod.boti;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.tardis.mod.boti.stores.BlockStore;
import net.tardis.mod.boti.stores.EntityStorage;
import net.tardis.mod.boti.stores.PlayerEntityStorage;
import net.tardis.mod.boti.stores.TileStore;
import net.tardis.mod.entity.TEntities;

/**
 * Author: Spectre0987
 * A class-agnostic object for handleing BOTI stuff
 */
public class BotiHandler {

    private List<TileStore> tileStores = Lists.newArrayList();
    private List<EntityStorage> entityStores = Lists.newArrayList();
    private AxisAlignedBB area;
    //Exclude these things for gathering so the doors and exteriors don't pickup themselves, doesn't need to be saved or sent, should be volitile and only matters on the server
    private BlockPos excludedTilePos = BlockPos.ZERO;
    private BlockPos excludedEntityPos = BlockPos.ZERO;
    private IBotiEnabled parent;

    public BotiHandler(IBotiEnabled parent){
        this.parent = parent;
    }

    public BotiHandler(IBotiEnabled parent, AxisAlignedBB area){
        this(parent);
        this.setArea(area);
    }

    public BotiHandler(IBotiEnabled parent, int diameter){
        this(parent);
        this.setDiameter(diameter);
    }

    public void setArea(AxisAlignedBB area){
        this.area = area;
    }

    /**
     * Set a square area to gather blocks from
     * @param diameter
     */
    public void setDiameter(int diameter){
        this.setArea(AxisAlignedBB.withSizeAtOrigin(diameter, diameter, diameter));
    }

    /**
     * For things to be centered, like scanner mode
     * @param world
     * @param center
     * @return
     */
    public boolean updateBoti(World world, BlockPos center){
        return this.updateBoti(world, center, BlockPos.ZERO, true);
    }

    /**
     *
     * @param otherWorld
     * @param center - set portal position
     * @param offset - Offset position that determines what blocks are captured in front of the portal.
     * @return
     */
    public boolean updateBoti(World otherWorld, BlockPos center, BlockPos offset, boolean isTardisInteriorWorld){
        //Must be on server
        if(!otherWorld.isRemote){
            //Setup World Shell if missing
            if(this.parent.getBotiWorld() == null){
                this.parent.setBotiWorld(new WorldShell(center, otherWorld));
            }

            if(!this.parent.getBotiWorld().getOffset().equals(center))
                this.parent.getBotiWorld().setOffset(center);

            //Gather blocks
            Map<BlockPos, BlockStore> newBlocks = this.gatherBlocks(otherWorld, center, area.offset(offset));

            /* Since we clear the blockstore map this is unneeded
            //Go through our existing blockstore map and see if anything changed, such as player removing blocks
            Map<BlockPos, BlockStore> diff = this.getDiff(this.parent.getBotiWorld().getMap(), newBlocks);
            //Update existing values in our new block map
            newBlocks.putAll(diff);
            */

            //Clear existing blockstores to fully remove blockstores that have become air
            this.parent.getBotiWorld().getBlockMap().clear();

            this.parent.getBotiWorld().getBlockMap().putAll(newBlocks);

            //Cull blocks out of range
            this.parent.getBotiWorld().cullOutOfRangeBlocks(isTardisInteriorWorld);
            
            List<EntityStorage> entities = this.gatherEntities(otherWorld, center, offset);
            this.entityStores.clear();
            this.entityStores.addAll(entities);

            
            return true;
        }
        return false;
    }

    /**
     * Gets all blocks and tiles in the area
     * @param world
     * @param pos
     * @param area
     * @return
     */
    public Map<BlockPos, BlockStore> gatherBlocks(World world, BlockPos pos, AxisAlignedBB area){

        //Make sure this is not a Mutable BlockPos, that will break this function
        pos = pos.toImmutable();

        Map<BlockPos, BlockStore> map = new HashMap<BlockPos, BlockStore>();

        //Clear pending tile messages
        this.tileStores.clear();
 
        for(int x = (int)Math.floor(area.minX); x < area.maxX; ++x){
            for(int y = (int)Math.floor(area.minY); y < area.maxY; ++y){
                for(int z = (int)Math.floor(area.minZ); z < area.maxZ; ++z){
                    BlockPos newPos = pos.add(x, y, z);
                    BlockState state = world.getBlockState(newPos);
                    if(!state.isAir(world, newPos) && !newPos.equals(this.excludedTilePos)){ //1.17: Use isAir method without any parameters
                        map.put(newPos, new BlockStore(world, newPos));
                    }

                    //Gather Tiles
                    TileEntity tile = world.getTileEntity(newPos);
                    if(tile != null && !newPos.equals(this.excludedTilePos)) {
                        this.tileStores.add(new TileStore(newPos, tile));
                    }
                }
            }
        }

        return map;
    }

    public List<EntityStorage> gatherEntities(World world, BlockPos center, BlockPos offset){
        List<EntityStorage> list = Lists.newArrayList();
        
        AxisAlignedBB box = this.area.offset(center).offset(offset);
         
    	//Normal Entities
        for(Entity e : world.getEntitiesWithinAABB(Entity.class, box)){
            //If this has an entity type
            if(e.getType() != null){

                //If this is a player (Unfinished)
                if(e instanceof PlayerEntity){
                    list.add(new PlayerEntityStorage((PlayerEntity)e));
                }
                else {
                	if (e.getType() != TEntities.CONTROL.get() && e.getType() != TEntities.CHAIR.get() && e.getType() != TEntities.TARDIS_BACKDOOR.get() && !e.getPosition().equals(this.excludedEntityPos))
                	    list.add(new EntityStorage(e));
                }
            }
        }  
        return list;
    }
    
    /**
     * Returns the difference in block stores between the two maps
     * @param original
     * @param newList
     * @return
     */
    public Map<BlockPos, BlockStore> getDiff(Map<BlockPos, BlockStore> original, Map<BlockPos, BlockStore> newList){
        Map<BlockPos, BlockStore> diff = Maps.newHashMap();

        for(Map.Entry<BlockPos, BlockStore> entry : newList.entrySet()){
            //If position is new
            if(!original.containsKey(entry.getKey())){
                diff.put(entry.getKey(), entry.getValue());
            }
            // if it existed, but the block is different now
            else if(!original.get(entry.getKey()).getState().matchesBlock(entry.getValue().getState().getBlock())){
                diff.put(entry.getKey(), entry.getValue());
            }
            // If player removed the block and material is different
            else if (!original.get(entry.getKey()).getState().getMaterial().equals(entry.getValue().getState().getMaterial())) {
                diff.put(entry.getKey(), entry.getValue());
            }
            // If player removed the block and it is now considered "air"
            else if (original.get(entry.getKey()).getState().getMaterial() == Material.AIR) {
                diff.put(entry.getKey(), entry.getValue());
            }
            //If neither, do not add
        }

        return diff;
    }
    
    public BlockPos getExcludedTilePos() {
        return this.excludedTilePos;
    }

    public void setExcludedTilePos(BlockPos pos){
        this.excludedTilePos = pos.toImmutable();
    }
    
    public BlockPos getExcludedEntityPos() {
        return this.excludedEntityPos;
    }
    
    public void setExcludedEntityPos(BlockPos pos) {
    	this.excludedEntityPos = pos.toImmutable();
    }

    public List<TileStore> getTileStores(){
        return this.tileStores;
    }

    public List<EntityStorage> getEntityStores(){
        return this.entityStores;
    }
    
    public IBotiEnabled getBotiEnabledObject() {
    	return this.parent;
    }

}
