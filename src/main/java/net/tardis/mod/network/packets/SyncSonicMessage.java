package net.tardis.mod.network.packets;

import java.util.UUID;
import java.util.function.Supplier;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.Hand;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.cap.items.sonic.SonicCapability;

/**
 * Created by Swirtzly
 * on 23/03/2020 @ 00:06
 */
public class SyncSonicMessage {

    public CompoundNBT data;
    public Hand hand;
    public UUID playerID;

    public SyncSonicMessage(Hand hand, CompoundNBT data, UUID playerID) {
        this.hand = hand;
        this.data = data;
        this.playerID = playerID;
    }

    public static void encode(SyncSonicMessage mes, PacketBuffer buffer) {
        buffer.writeInt(mes.hand.ordinal());
        buffer.writeCompoundTag(mes.data);
        buffer.writeUniqueId(mes.playerID);
    }

    public static SyncSonicMessage decode(PacketBuffer buffer) {
        return new SyncSonicMessage(Hand.values()[buffer.readInt()], buffer.readCompoundTag(), buffer.readUniqueId());
    }

    public static void handle(SyncSonicMessage mes, Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
        	PlayerEntity player = Minecraft.getInstance().world.getPlayerByUuid(mes.playerID);
        	if(player != null) {
        		SonicCapability.getForStack(player.getHeldItem(mes.hand)).ifPresent(cap -> cap.deserializeNBT(mes.data));
        	}
        });
        ctx.get().setPacketHandled(true);
    }

}
