package net.tardis.mod.network.packets;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import com.google.common.collect.Lists;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.boti.stores.EntityStorage;
import net.tardis.mod.client.ClientPacketHandler;

public class BOTIEntityMessage {

	public List<EntityStorage> entities = Lists.newArrayList();
	public List<EntityStorage> interiorWorldEntities = Lists.newArrayList(); //Seperate list for entities inside the interior dimension to prevent CME error
	public BlockPos pos;
	public int type = 0;
	
	public BOTIEntityMessage(BlockPos pos, List<EntityStorage> gatheredEntities) {
		this.pos = pos;
		this.type = 0;
		this.interiorWorldEntities = Lists.newArrayList(gatheredEntities);
	}
	
	public BOTIEntityMessage(List<EntityStorage> gatheredEntities) {
		this.type = 1;
		this.entities = Lists.newArrayList(gatheredEntities);
	}
	

	public static void encode(BOTIEntityMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.type);
		if(mes.type == 0) {
			buf.writeBlockPos(mes.pos);
			buf.writeInt(mes.interiorWorldEntities.size());
			for(EntityStorage store : mes.interiorWorldEntities) {
				store.encode(buf);
			}
		}
		else {
			buf.writeInt(mes.entities.size());
			for(EntityStorage store : mes.entities) {
				store.encode(buf);
			}
		}
	}
	
	public static BOTIEntityMessage decode(PacketBuffer buf) {
		BlockPos pos = null;
		List<EntityStorage> list = Lists.newArrayList();
		int type = buf.readInt();
        
		if(type == 0)
			pos = buf.readBlockPos();

		int size = buf.readInt();

		for(int i = 0; i < size; ++i) {
			list.add(new EntityStorage(buf));
		}
		if(pos == null || type == 1)
			return new BOTIEntityMessage(list);
		
		return new BOTIEntityMessage(pos, list);
	}
	
	
	public static void handle(BOTIEntityMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			List<EntityStorage> entities = new ArrayList<>();
			if (mes.type == 0) {
				entities.addAll(mes.interiorWorldEntities);
			}
			else {
				entities.addAll(mes.entities);
			}
			ClientPacketHandler.handleWorldShellEntity(mes.type, entities, mes.pos);
		});
		context.get().setPacketHandled(true);
	}
}
