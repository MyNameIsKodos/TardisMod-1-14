package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;
import net.tardis.mod.world.dimensions.TDimensions;

public class SnapMessage {

	public SnapMessage() {}
	
	public static void encode(SnapMessage mes, PacketBuffer buf) {}
	
	public static SnapMessage decode(PacketBuffer buf) {
		return new SnapMessage();
	}
	
	public static void handle(SnapMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			ServerWorld world = context.get().getSender().getServerWorld();
			MinecraftServer server = context.get().getSender().getServer();
			if (WorldHelper.areDimensionTypesSame(world, TDimensions.DimensionTypes.TARDIS_TYPE)) {
				TardisHelper.getConsole(server, world).ifPresent(tile -> {
						tile.getDoor().ifPresent(door -> {
						int range = 16;
						if(door.getPosition().withinDistance(context.get().getSender().getPosition(), range)) {
							door.setOpenState(door.getOpenState() != EnumDoorState.CLOSED ? EnumDoorState.CLOSED : EnumDoorState.BOTH);
							door.updateExteriorDoorData();
							context.get().getSender().world.playSound(null, context.get().getSender().getPosition(), TSounds.SNAP.get(), SoundCategory.PLAYERS, 0.2F, 1F);
							if (door.getOpenState().equals(EnumDoorState.CLOSED)) {
								context.get().getSender().world.playSound(null, context.get().getSender().getPosition(), tile.getExteriorType().getDoorSounds().getClosedSound(), SoundCategory.PLAYERS, 0.5F, 1F);
		                    }
		                    else {
		                    	context.get().getSender().world.playSound(null, context.get().getSender().getPosition(), tile.getExteriorType().getDoorSounds().getOpenSound(), SoundCategory.BLOCKS, 0.5F, 1F);
		                    }
						}
					});
				});	
			}
			else { //If not in the Tardis dimension
				for (ServerWorld interior : TardisHelper.getTardises(server)) {
					TardisHelper.getConsoleInWorld(interior).ifPresent(tile -> {
						if(tile.getEmotionHandler().getLoyaltyTrackingCrew().contains(context.get().getSender().getUniqueID()) && tile.getEmotionHandler().getLoyalty(context.get().getSender().getUniqueID()) > 100) {
							int consoleRange = 20;
							boolean withinDistanceOfExterior = context.get().getSender().getPosition().withinDistance(tile.getCurrentLocation(), consoleRange);
							if(withinDistanceOfExterior) {
								ExteriorTile ext = tile.getExteriorType().getExteriorTile(tile);
								if(ext != null) {
									if(ext.getLocked())
										ext.setLocked(false);
									ext.setDoorState(ext.getOpen() != EnumDoorState.CLOSED ? EnumDoorState.CLOSED : EnumDoorState.BOTH);
									ext.copyDoorStateToInteriorDoor();
									context.get().getSender().world.playSound(null, context.get().getSender().getPosition(), TSounds.SNAP.get(), SoundCategory.PLAYERS, 0.2F, 1F);
									if (ext.getOpen().equals(EnumDoorState.CLOSED)) {
										context.get().getSender().world.playSound(null, context.get().getSender().getPosition(), tile.getExteriorType().getDoorSounds().getClosedSound(), SoundCategory.PLAYERS, 0.5F, 1F);
				                    }
				                    else {
				                    	context.get().getSender().world.playSound(null, context.get().getSender().getPosition(), tile.getExteriorType().getDoorSounds().getOpenSound(), SoundCategory.BLOCKS, 0.5F, 1F);
				                    }
								}
							}
						}
					});
				}
			}
		});
		context.get().setPacketHandled(true);
	}
}
