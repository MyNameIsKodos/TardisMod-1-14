package net.tardis.mod.network.packets;

import java.util.UUID;
import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.entity.humanoid.CompanionEntity;
import net.tardis.mod.enums.EnumCompanionAction;

public class CompanionActionMessage {
	
	private EnumCompanionAction type;
	private UUID entityID;
	
	public CompanionActionMessage(UUID entityID, EnumCompanionAction type) {
		this.entityID = entityID;
		this.type = type;
	}
	
	public static void encode(CompanionActionMessage mes, PacketBuffer buff) {
		
		buff.writeUniqueId(mes.entityID);
		buff.writeInt(mes.type.ordinal());
	}
	
	public static CompanionActionMessage decode(PacketBuffer buff) {
		UUID entityID = buff.readUniqueId();
		EnumCompanionAction type = EnumCompanionAction.values()[buff.readInt()];
		return new CompanionActionMessage(entityID, type);
	}
	
	public static void handle(CompanionActionMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			CompanionEntity comp = (CompanionEntity) cont.get().getSender().getServerWorld().getEntityByUuid(mes.entityID);
			if (comp != null) {
				if (!cont.get().getSender().world.isRemote()) {
					if (mes.type == EnumCompanionAction.FLY_TARDIS) {
						comp.setFlyingTardis(true);
						comp.setSitting(true); //Set "sitting" to true so that the companion doesn't try to follow the player. This also allows the "Follow me" prompt is shown the next time we click the gui.
					}
					if (mes.type == EnumCompanionAction.SITTING) {
						comp.setSitting(!comp.isSitting());
						comp.setFlyingTardis(false);
					}
				}
			}
		});
		cont.get().setPacketHandled(true);
	}
}
