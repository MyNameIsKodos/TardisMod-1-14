package net.tardis.mod.network.packets.data.Requestors;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.MaterializationMessage;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class MaterializationRequestor extends Requestor<ExteriorTile> {

    public MaterializationRequestor() {
        super(ExteriorTile.class);
    }

    @Override
    public void act(ExteriorTile tile, ServerPlayerEntity sender) {
        Network.sendTo(new MaterializationMessage(tile.getPos(), tile.getMatterState(), tile.getMaterializationTicks()), sender);
    }
}
