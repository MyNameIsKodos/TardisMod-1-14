package net.tardis.mod.items.misc;

import net.minecraft.util.math.vector.Vector3d;

public enum LaserPowerTier {
    LOW(8, 20, 0.75F, new Vector3d(1,0,0), 1F, 0.8F, 5, "low"),
    MEDIUM(14, 40, 1.5F, new Vector3d(0,1,0), 2F, 1F, 10, "medium"),
    HIGH(20, 60, 1.75F, new Vector3d(0,0,1), 3F, 1.25F, 20, "high");
    
    float damageInflicted = 5F;
    float chargeRequired = 0F;
    int cooldownTicks = 20;
    float velocity = 1.5F;
    Vector3d laserColour = new Vector3d(1D, 1D, 1D);
    float soundPitch = 1F;
    float laserLength = 2F;
    String translationKey = "power_tier.tardis";
    
    LaserPowerTier(float damage, float chargeRequired, float velocity, Vector3d laserColour, float laserLength, float soundPitch, int cooldownTicks, String translationKey){
        this.damageInflicted = damage;
        this.chargeRequired = chargeRequired;
        this.velocity = velocity;
        this.laserColour = laserColour;
        this.laserLength = laserLength;
        this.soundPitch = soundPitch;
        this.cooldownTicks = cooldownTicks;
        this.translationKey = "laser_power_tier.tardis." + translationKey;
    }
    
    public float getDamageInflicted() {
        return this.damageInflicted;
    }
    
    public float getChargeRequired() {
        return this.chargeRequired;
    }
    
    public float getVelocity() {
        return this.velocity;
    }
    
    public Vector3d getLaserColour() {
        return this.laserColour;
    }
    
    public float getLaserLength() {
        return this.laserLength;
    }
    
    public float getSoundPitch() {
        return this.soundPitch;
    }
    
    public int getCooldownTicks() {
        return this.cooldownTicks;
    }
    
    public String getTranslationKey() {
        return this.translationKey;
    }
}
