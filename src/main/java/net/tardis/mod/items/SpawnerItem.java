package net.tardis.mod.items;

import java.util.function.Supplier;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemUseContext;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.tardis.mod.helper.WorldHelper;

public class SpawnerItem extends BaseItem {

	Supplier<EntityType<?>> type;

    public SpawnerItem(Supplier<EntityType<?>> type) {
        super(new Properties().group(ItemGroup.MISC));
        this.type = type;
    }

    @Override
    public ActionResultType onItemUse(ItemUseContext context) {
        if (context.getFace() == Direction.UP) {
            if (!context.getWorld().isRemote()) {
                Entity e = type.get().create(context.getWorld());

                double x = 0, z = 0;

                if (context.getPlayer().isSneaking()) {
                    double angle = Math.toRadians(WorldHelper.getAngleFromFacing(context.getPlacementHorizontalFacing().rotateY()));
                    x = Math.sin(angle) * 0.5;
                    z = -Math.cos(angle) * 0.5;
                }

                e.setPosition(context.getPos().getX() + x + 0.5, context.getHitVec().y, context.getPos().getZ() + z + 0.5);
                e.rotationYaw = WorldHelper.getAngleFromFacing(context.getPlayer().getHorizontalFacing().getOpposite());
                context.getWorld().addEntity(e);
                context.getItem().shrink(1);
            }
            return ActionResultType.SUCCESS;
        }
        return ActionResultType.PASS;
    }
}
