package net.tardis.mod.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.tardis.mod.blocks.ChairBlock;

public class ChairEntity extends Entity{

	private static final DataParameter<Float> MOUNTED_OFFSET = EntityDataManager.createKey(ChairEntity.class, DataSerializers.FLOAT);
	
	public ChairEntity(EntityType<?> entityTypeIn, World worldIn) {
		super(entityTypeIn, worldIn);
	}

	public ChairEntity(World worldIn) {
		this(TEntities.CHAIR.get(), worldIn);
	}

	
	@Override
	protected void registerData() {
		this.getDataManager().register(MOUNTED_OFFSET, 0.5F);
	}

	@Override
	protected void readAdditional(CompoundNBT compound) {
		this.getDataManager().set(MOUNTED_OFFSET, compound.getFloat("mounted_offset"));
	}

	@Override
	public void tick() {
		super.tick();
		if(this.ticksExisted > 20 && (this.getPassengers().isEmpty() || !(world.getBlockState(getPosition()).getBlock() instanceof ChairBlock)))
			remove();
	}

	@Override
	protected boolean canFitPassenger(Entity passenger) {
		return this.getPassengers().size() < 1;
	}

	@Override
	protected void writeAdditional(CompoundNBT compound) {
		compound.putFloat("mounted_offset", this.getDataManager().get(MOUNTED_OFFSET));
	}

	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}
	
	@Override
	public double getMountedYOffset() {
		return (double) this.getDataManager().get(MOUNTED_OFFSET);
	}
	
	public void setYOffset(float y) {
		this.getDataManager().set(MOUNTED_OFFSET, y);
	}

}
