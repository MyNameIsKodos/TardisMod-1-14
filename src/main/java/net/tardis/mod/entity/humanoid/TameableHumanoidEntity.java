package net.tardis.mod.entity.humanoid;

import java.util.UUID;

import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.entity.IPlayerTameable;
import net.tardis.mod.entity.TameStates;
import net.tardis.mod.missions.misc.Dialog;
/** Template for tameable humanoid entity. 
 * <br> Has a generic Tamed data parameter which can hold substates of:
 * <p> -Tamed
 * <br> -Sleeping*/
public class TameableHumanoidEntity extends AbstractHumanoidEntity implements IPlayerTameable{
	public static final DataParameter<Byte> TAMED = EntityDataManager.createKey(TameableHumanoidEntity.class, DataSerializers.BYTE);
	protected boolean sitting;
	protected UUID owner = null;
	protected PlayerEntity ownerPlayer;
	
	public TameableHumanoidEntity(EntityType<? extends CreatureEntity> type, World worldIn) {
		super(type, worldIn);

	}
	
	@Override
	protected void registerData() {
		super.registerData();
		this.getDataManager().register(TAMED, (byte)0);
	}

	@Override
	public void readAdditional(CompoundNBT compound) {
		super.readAdditional(compound);
		if (compound.contains("owner_id"))
		    this.owner = compound.getUniqueId("owner_id");
		setSitting(compound.getBoolean("is_sitting"));
	}

	@Override
	public void writeAdditional(CompoundNBT compound) {
		super.writeAdditional(compound);
		if (this.owner != null)
            compound.putUniqueId("owner_id", this.owner);
		compound.putBoolean("is_sitting", this.isSitting());
	}

	@Override
	public Dialog getCurrentDialog(PlayerEntity player) {
		return null;
	}

	@Override
	public boolean isTamed() {
		boolean isTamedStatus = (this.getDataManager().get(TAMED) & TameStates.TAMED) != 0;
        return this.owner != null && isTamedStatus;
	}

	@Override
	public void setTamed(boolean tamed) {
		byte currentTameState = this.dataManager.get(TAMED);
		if (tamed) {
			this.getDataManager().set(TAMED, (byte)(currentTameState | TameStates.TAMED));
		}
		else {
			this.getDataManager().set(TAMED, (byte)(currentTameState & TameStates.NOT_TAMED));
		}
	}

	@Override
	public UUID getOwner() {
		return this.owner;
	}

	@Override
	public void setOwner(UUID owner) {
		this.owner = owner;
	}
	
	public PlayerEntity getOwnerEntity() {
		
		if(this.owner == null)
			return null;
		
		if(this.ownerPlayer != null && this.ownerPlayer.isAlive())
			return ownerPlayer;
		
		if(!world.isRemote) {
			Entity ent = ((ServerWorld)world).getEntityByUuid(owner);
			if(ent instanceof PlayerEntity)
				return this.ownerPlayer = (PlayerEntity)ent;
		}
		return null;
	}
	
	@Override
	public ActionResultType getEntityInteractionResult(PlayerEntity player, Hand hand) {
		if (!player.world.isRemote() && this.getOwnerEntity() == null) {
		    this.owner = player.getUniqueID();
		    this.ownerPlayer = player;
		    this.setTamed(true);
		}
		return super.getEntityInteractionResult(player, hand);
	}

	@Override
	public ResourceLocation getSkin() {
		return new ResourceLocation("textures/entity/steve.png");
	}

	@Override
	public boolean isSitting() {
		byte currentTameState = this.dataManager.get(TAMED);
		boolean isSitting = (currentTameState & TameStates.TAMED_SITTING) != 0;
		this.sitting = isSitting;
		return this.sitting;
	}

	@Override
	public void setSitting(boolean sit) {
		this.sitting = sit;
		byte currentTameState = this.dataManager.get(TAMED);
		if (sit) {
			//Add 2 to decimal value
			this.getDataManager().set(TAMED, (byte)(currentTameState | TameStates.TAMED_SITTING));
		}
		else {
			//Remain the same decimal value
			this.getDataManager().set(TAMED, (byte)(currentTameState & TameStates.TAMED_NOT_SITTING));
		}
	}
	
	public boolean isSleeping() {
		return (this.getDataManager().get(TAMED) & TameStates.TAMED_SLEEPING) != 0;
	}
	
	public void setSleepingPose(boolean sleep) {
		byte currentTameState = this.dataManager.get(TAMED);
		if (sleep) {
			//Add 1 to decimal value
			this.getDataManager().set(TAMED, (byte)(currentTameState | TameStates.TAMED_SLEEPING));
		}
		else {
			//Remain the same decimal value
			this.getDataManager().set(TAMED, (byte)(currentTameState & TameStates.TAMED_NOT_SLEEPING));
		}
	}

}
