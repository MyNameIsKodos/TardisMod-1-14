package net.tardis.mod.entity.humanoid;

import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.goal.AvoidEntityGoal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomWalkingGoal;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.ai.humanoids.CompanionFollowGoal;
import net.tardis.mod.entity.ai.humanoids.HelpFlyTARDISGoal;
import net.tardis.mod.entity.hostile.dalek.DalekEntity;
import net.tardis.mod.enums.EnumCompanionAction;
import net.tardis.mod.missions.misc.Dialog;
import net.tardis.mod.missions.misc.DialogOption;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.CompanionActionMessage;

public class CompanionEntity extends TameableHumanoidEntity{	
	
	public boolean isFlyingTardis = false;
	
	public CompanionEntity(EntityType<? extends CreatureEntity> type, World worldIn) {
		super(type, worldIn);
	}
	
	public CompanionEntity(World worldIn) {
		this(TEntities.COMPANION.get(), worldIn);
	}

	@Override
	public Dialog getCurrentDialog(PlayerEntity player) {
		
		Dialog root = new Dialog(getCompanionDialogKey("hello"));
		
		//Follow type
		DialogOption followType = new DialogOption(null, !this.isSitting() ? getCompanionDialogOptionKey("wait") : getCompanionDialogOptionKey("follow"));
		followType.setOptionAction((companion, player1) -> {
			CompanionEntity comp = (CompanionEntity)companion;
			Network.sendToServer(new CompanionActionMessage(comp.getUniqueID(), EnumCompanionAction.SITTING));
		});
		DialogOption learnConsole = new DialogOption(null, getCompanionDialogOptionKey("fly_tardis"));
		learnConsole.setOptionAction((companion, player2) -> {
			CompanionEntity comp = (CompanionEntity)companion;
			Network.sendToServer(new CompanionActionMessage(comp.getUniqueID(), EnumCompanionAction.FLY_TARDIS));
		});
		root.addDialogOption(learnConsole);
		root.addDialogOption(followType);
		
		return root;
	}

	@Override
	public ResourceLocation getSkin() {
		return new ResourceLocation("textures/entity/steve.png");
	}

	@Override
	protected void registerGoals() {
		super.registerGoals();
		this.goalSelector.addGoal(2, new HelpFlyTARDISGoal(this, 0.4D, 16));
		this.goalSelector.addGoal(3, new CompanionFollowGoal(this, 0.4D));
		this.goalSelector.addGoal(5, new AvoidEntityGoal<>(this, DalekEntity.class, 6F, 0.4D, 0.4D)); 
		this.goalSelector.addGoal(8, new WaterAvoidingRandomWalkingGoal(this, 0.3D));
		
	}

	public static AttributeModifierMap.MutableAttribute createAttributes() {
    	return CreatureEntity.func_233666_p_().createMutableAttribute(Attributes.MAX_HEALTH, 80D)
    			.createMutableAttribute(Attributes.ARMOR_TOUGHNESS, 20D)
    			.createMutableAttribute(Attributes.MOVEMENT_SPEED, 1.0D);
    }
	
	@Override
	public void readAdditional(CompoundNBT compound) {
		super.readAdditional(compound);
		setFlyingTardis(compound.getBoolean("flying_tardis"));
	}

	@Override
	public void writeAdditional(CompoundNBT compound) {
		super.writeAdditional(compound);
		compound.putBoolean("flying_tardis", this.isFlyingTardis());
	}
	
	public boolean isFlyingTardis() {
		return this.isFlyingTardis;
	}
	
	public void setFlyingTardis(boolean isFlyingTardis) {
		this.isFlyingTardis = isFlyingTardis;
	}
	
	public String getCompanionDialogKey(String key) {
		return "companion.tardis.dialog." + key;
	}
	
	public String getCompanionDialogOptionKey(String key) {
		return "companion.tardis.dialog_option." + key;
	}
	
}
