package net.tardis.mod.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntitySize;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.Pose;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.util.DamageSource;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.WorldHelper;
/** Entity to allow entities to teleport to its stored interior world*/
public class TardisBackdoorEntity extends Entity{
    
    private RegistryKey<World> interiorWorldKey;

    public TardisBackdoorEntity(EntityType<?> entityTypeIn, World worldIn) {
        super(entityTypeIn, worldIn);
    }
    
    public TardisBackdoorEntity(World worldIn) {
        super(TEntities.TARDIS_BACKDOOR.get(), worldIn);
    }

    @Override
    protected void registerData() {
    }

    @Override
    protected void readAdditional(CompoundNBT compound) {
        this.interiorWorldKey = WorldHelper.getWorldKeyFromRL(new ResourceLocation(compound.getString("interior_world_key")));
    }

    @Override
    protected void writeAdditional(CompoundNBT compound) {
        if (this.interiorWorldKey != null)
            compound.putString("interior_world_key", this.interiorWorldKey.getLocation().toString());
    }

    @Override
    public IPacket<?> createSpawnPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    @Override
    public void onCollideWithPlayer(PlayerEntity player) {
        if (!this.world.isRemote()) {
            if (this.interiorWorldKey != null) {
                WorldHelper.teleportEntities(player, world.getServer().getWorld(interiorWorldKey), TardisHelper.TARDIS_POS.up(), player.rotationYaw, player.rotationPitch);
                this.world.playSound(null, this.getPosition(), SoundEvents.ITEM_CHORUS_FRUIT_TELEPORT, SoundCategory.PLAYERS, 1F, 1F);
                player.world.playSound(null, player.getPosition(), SoundEvents.ITEM_CHORUS_FRUIT_TELEPORT, SoundCategory.PLAYERS, 1F, 1F);
                this.remove();
            }
        }
    }

    @Override
	public EntitySize getSize(Pose poseIn) {
		return super.getSize(poseIn);
	}

	public RegistryKey<World> getInteriorWorldKey(){
        return interiorWorldKey;
    }
    
    public void setInteriorWorldKey(RegistryKey<World> interiorWorldKey) {
        this.interiorWorldKey = interiorWorldKey;
    }

    @Override
    public boolean isImmuneToFire() {
        return true;
    }

    @Override
    public boolean isInvulnerableTo(DamageSource source) {
        return super.isInvulnerableTo(source);
    }

    @Override
    public boolean isImmuneToExplosions() {
        return true;
    }

}
