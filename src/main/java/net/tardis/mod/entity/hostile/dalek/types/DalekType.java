package net.tardis.mod.entity.hostile.dalek.types;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.goal.BreakDoorGoal;
import net.minecraft.entity.ai.goal.HurtByTargetGoal;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.ai.goal.LookRandomlyGoal;
import net.minecraft.entity.ai.goal.MoveTowardsRaidGoal;
import net.minecraft.entity.ai.goal.OpenDoorGoal;
import net.minecraft.entity.ai.goal.PrioritizedGoal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomWalkingGoal;
import net.minecraft.entity.item.ArmorStandEntity;
import net.minecraft.entity.merchant.villager.AbstractVillagerEntity;
import net.minecraft.entity.monster.AbstractRaiderEntity;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.monster.PatrollerEntity;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.Difficulty;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.damagesources.TDamageSources;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.ai.FlyingMovementGoal;
import net.tardis.mod.entity.ai.TardisMoveTowardsRaidGoal;
import net.tardis.mod.entity.ai.TardisRaiderOpenGoal;
import net.tardis.mod.entity.ai.dalek.DalekAttackGoal;
import net.tardis.mod.entity.hostile.dalek.DalekEntity;
import net.tardis.mod.entity.hostile.dalek.weapons.DalekWeapon;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.AbstractWeapon;
import net.tardis.mod.sounds.TSounds;

public class DalekType extends ForgeRegistryEntry<DalekType> {

	private SoundEvent[] ambientSounds = null;
    private SoundEvent[] deathSounds = null;
    private SoundEvent[] fireSounds = null;
    private List<String> textureVariants = new ArrayList<>();
    private AbstractWeapon<DalekEntity> dalekWeapon = null;
    private boolean canFly = true;
    private double maxHealth = 60D;
    private double followRange = 60D;
    private double movementSpeed = 0.55D;
    private double attackDamage = 5D;
    private double attackKnockback = 0.5D;
    private double attacksPerSecond = 1.0D;
    private double flyingSpeed = 0.8D;
    private double knockbackResistance = 0.1D;
    private boolean removeOldGoals = false;
    private boolean removeTargetGoals = false;

    //Client side related variables
    private Vector3d laserColour = new Vector3d(0,0,1);
    private Item spawnItem = TItems.SPAWN_EGG_DALEK_DEFAULT.get();
    
    /** Adds an additional texture variant
     * <br> Add the file name of texture file. This must be a .png file type
     * The texture file must be under:
     * <br> assets/your_modid/textures/entity/dalek/ as defined by the DalekRenderer*/
    public DalekType addTextureVariant(String textureVariantFileName) {
        textureVariants.add(textureVariantFileName);
        return this;
    }

    /** Adds a list of additional texture variants
     * <br> Add the file name of texture file. This must be a .png file type
     * The texture file must be under:
     * <br> assets/your_modid/textures/entity/dalek/ as defined by the DalekRenderer*/
    public DalekType addTextureVariant(List<String> textureVariantFileNames) {
        this.textureVariants.addAll(textureVariantFileNames);
        return this;
    }
   
    /** Removes old goals if applicable, adds new goals and modifies the attributes of the Dalek*/
    public DalekType setupDalek(DalekEntity dalekEntity) {
        //Tasks
    	if (removeOldGoals) {
    		Set<PrioritizedGoal> goalsToRemove = new HashSet<>();
    		for (PrioritizedGoal goal : dalekEntity.goalSelector.goals) {
    			goalsToRemove.add(goal);
            }
    		
    		for (PrioritizedGoal pGoal : goalsToRemove) {
        		dalekEntity.goalSelector.removeGoal(pGoal);
        	}
    	}
        
    	if (removeTargetGoals) {
    		Set<PrioritizedGoal> goalsToRemove = new HashSet<>();
    		
    		for (PrioritizedGoal goal : dalekEntity.targetSelector.goals) {
                dalekEntity.targetSelector.removeGoal(goal);
            }
    		for (PrioritizedGoal pGoal : goalsToRemove) {
        		dalekEntity.targetSelector.removeGoal(pGoal);
        	}
    	}
        
        this.addGoals(dalekEntity);
        this.modifyExistingAttributes(dalekEntity);
        this.setupWeapons(dalekEntity);
        return this;
    }

    /** Adds goals to this Dalek type*/
    protected void addGoals(DalekEntity dalekEntity) {
    	Set<PrioritizedGoal> goalsToRemove = new HashSet<>();
    	
    	for (PrioritizedGoal goal : dalekEntity.goalSelector.goals) { //Remove the patrol goal so that the movement speed is that of our Dalek type
    		//Add these goals to be removed later, then readd them ourselves later manually as the vanilla values will make daleks move too fast
    		if (goal.getGoal() instanceof PatrollerEntity.PatrolGoal || goal.getGoal() instanceof AbstractRaiderEntity.InvadeHomeGoal || goal.getGoal() instanceof MoveTowardsRaidGoal) {
    			if (!goal.isRunning())
    				goalsToRemove.add(goal);
    		}
        }
    	
    	//Have another loop to prevent CME
    	for (PrioritizedGoal pGoal : goalsToRemove) {
    		dalekEntity.goalSelector.goals.remove(pGoal); //Have to use the underlying raw set as the removeGoal method is called async is delayed far too late to be of any use
    	}
    	
    	dalekEntity.goalSelector.addGoal(1, new DalekAttackGoal(dalekEntity, LivingEntity.class, true, false, 20D));
        dalekEntity.targetSelector.addGoal(1, (new HurtByTargetGoal(dalekEntity)).setCallsForHelp(DalekEntity.class));
        dalekEntity.goalSelector.addGoal(3, new TardisMoveTowardsRaidGoal(dalekEntity, this.movementSpeed * 1.25D));
        if (this.canFly())
            dalekEntity.goalSelector.addGoal(7, new FlyingMovementGoal(dalekEntity));
        dalekEntity.goalSelector.addGoal(4, new PatrollerEntity.PatrolGoal<>(dalekEntity, (this.movementSpeed), this.movementSpeed));
        dalekEntity.goalSelector.addGoal(5, new AbstractRaiderEntity.InvadeHomeGoal(dalekEntity, this.movementSpeed * 1.25D, 20));
        dalekEntity.goalSelector.addGoal(6, new TardisRaiderOpenGoal(dalekEntity));
        dalekEntity.goalSelector.addGoal(6, new OpenDoorGoal(dalekEntity, false));
        dalekEntity.goalSelector.addGoal(6, new BreakDoorGoal(dalekEntity, (flag) -> flag == Difficulty.HARD));
        dalekEntity.goalSelector.addGoal(8, new WaterAvoidingRandomWalkingGoal(dalekEntity, (this.movementSpeed)));
        dalekEntity.goalSelector.addGoal(9, new LookAtGoal(dalekEntity, PlayerEntity.class, 8.0F));
        dalekEntity.goalSelector.addGoal(9, new LookRandomlyGoal(dalekEntity));
    }

    /** This is private because you should be overriding the various getters such as getMaxHealth in your DalekType*/
    private void modifyExistingAttributes(DalekEntity dalekEntity) {
    	//Attributes - can only modify the attributes DalekEntity has
        dalekEntity.getAttribute(Attributes.MAX_HEALTH).setBaseValue(this.maxHealth);
        dalekEntity.getAttribute(Attributes.FOLLOW_RANGE).setBaseValue(this.followRange);
        dalekEntity.getAttribute(Attributes.MOVEMENT_SPEED).setBaseValue(this.movementSpeed);
        dalekEntity.getAttribute(Attributes.ATTACK_DAMAGE).setBaseValue(this.attackDamage);
        dalekEntity.getAttribute(Attributes.ATTACK_KNOCKBACK).setBaseValue(this.attackKnockback);
        dalekEntity.getAttribute(Attributes.ATTACK_SPEED).setBaseValue(this.attacksPerSecond);
        dalekEntity.getAttribute(Attributes.FLYING_SPEED).setBaseValue(this.flyingSpeed);
        dalekEntity.getAttribute(Attributes.KNOCKBACK_RESISTANCE).setBaseValue(this.knockbackResistance);
    }
    
    protected void setupWeapons(DalekEntity dalek) {
    	this.setWeapon(new DalekWeapon(dalek));
    }

    /** Handle special behaviour*/
    public void tickSpecial(DalekEntity dalekEntity) {
    
    }
    
    public double getMaxHealth() {
    	return this.maxHealth;
    }
    
    public double getFollowRange() {
    	return this.followRange;
    }
    
    public double getMovementSpeed() {
    	return this.movementSpeed;
    }
    
    public double getAttackDamage() {
    	return this.attackDamage;
    }
    
    public double getAttackKnockback() {
    	return this.attackKnockback;
    }

    /** Get the number of attacks that can happen in one second*/
    public double getAttacksPerSecond() {
    	return this.attacksPerSecond;
    }
    
    public double getFlyingSpeed() {
    	return this.flyingSpeed;
    }
    
    public double getKnockbackResistance() {
    	return this.knockbackResistance;
    }
    
    public DamageSource getDamageSource(LivingEntity entity) {
    	return TDamageSources.causeTardisMobDamage(TDamageSources.DALEK, entity);
    }
    
    public Vector3d getLaserColour() {
    	return this.laserColour;
    }
    
    public boolean canFly() {
    	return this.canFly;
    }
    
    public DalekType setCanFly(boolean canFly) {
    	this.canFly = canFly;
    	return this;
    }

    //Sounds
    public SoundEvent getAmbientSound(DalekEntity dalekEntity) {
        if (ambientSounds == null) {
            ambientSounds = new SoundEvent[]{TSounds.DALEK_HOVER.get(), TSounds.DALEK_ARM.get()};
        }
        return ambientSounds[(int) (System.currentTimeMillis() % ambientSounds.length)];
    }

    public SoundEvent getFireSound(DalekEntity dalekEntity) {
        if (fireSounds == null) {
            fireSounds = new SoundEvent[]{TSounds.DALEK_FIRE.get(), TSounds.DALEK_FIRE_EXTENDED.get(), TSounds.DALEK_EXTERMINATE.get()};
        }
        return fireSounds[(int) (System.currentTimeMillis() % fireSounds.length)];
    }

    public SoundEvent getDeathSound(DalekEntity dalekEntity) {
        if (deathSounds == null) {
            deathSounds = new SoundEvent[]{TSounds.DALEK_DEATH.get()};
        }
        return deathSounds[(int) (System.currentTimeMillis() % deathSounds.length)];
    }

    public String getRandomTexture(DalekEntity dalekEntity) {
        return textureVariants.get(dalekEntity.world.rand.nextInt(textureVariants.size()));
    }

    public Predicate<LivingEntity> getAttackPredicate() {
        return livingEntity -> {
        	boolean predicate = livingEntity.getType() != TEntities.DALEK.get() && !(livingEntity instanceof AbstractRaiderEntity) && !(livingEntity instanceof ArmorStandEntity) && livingEntity.getType() != TEntities.HOLO_PILOT.get();
        	boolean dalekTargetAll = TConfig.SERVER.dalekTargetAllLiving.get();
        	if (!dalekTargetAll) { //If we don't want the dalek to target all living entities.
        		if (TConfig.SERVER.dalekTargetHostileOnly.get()) {
        			if (livingEntity instanceof CreatureEntity) { //If we look in creatures, only look for monster
        				predicate = predicate && livingEntity instanceof MonsterEntity;
        			}
        			else { //Else, look in mobentity types, this will include entities like ender dragons and possibly mobs added by other mods
        				predicate = predicate && livingEntity instanceof MobEntity;
        			}
        		}
        		else { //If able to target hostiles but we want to blacklist or whitelist specific entity instances
        			if (!TConfig.SERVER.dalekTargetAnimals.get()) {
        				predicate = predicate && !(livingEntity instanceof AnimalEntity);
            		}
            		if (!TConfig.SERVER.dalekTargetVillagers.get()) {
            			predicate = predicate && !(livingEntity instanceof AbstractVillagerEntity);
            		}
            		if (!TConfig.SERVER.dalekTargetPlayers.get()) {
                		predicate = predicate && !(livingEntity instanceof PlayerEntity);
            		}
        		}
        		return predicate;
        	}
        	else {
        		return predicate;
        	}
        };
    }

    public DalekType setWeapon(AbstractWeapon<DalekEntity> weapon) {
        this.dalekWeapon = weapon;
        return this;
    }

    public AbstractWeapon<DalekEntity> getWeapon() {
        return dalekWeapon;
    }
    
    public Item getSpawnItem() {
        return spawnItem;
    }

    public DalekType setSpawnItem(Item spawnItem) {
        this.spawnItem = spawnItem;
        return this;
    }
    
    public boolean removeOldGoals() {
    	return this.removeOldGoals;
    }
    
    public void setRemoveOldGoals(boolean removeOldGoals) {
    	this.removeOldGoals = removeOldGoals;
    }
    
    public boolean removeTargetGoals() {
    	return this.removeTargetGoals;
    }
    
    public void setRemoveTargetGoals(boolean removeTargetGoals) {
    	this.removeTargetGoals = removeTargetGoals;
    }


}
