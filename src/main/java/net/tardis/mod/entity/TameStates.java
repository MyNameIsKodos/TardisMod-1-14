package net.tardis.mod.entity;

public class TameStates {
    public static final int TAMED = 4;
    public static final int NOT_TAMED = -5;
    public static final int TAMED_NOT_SLEEPING = -2;
    public static final int TAMED_SLEEPING = 1;
    public static final int TAMED_NOT_SITTING = -3;
    public static final int TAMED_SITTING = 2;
}
