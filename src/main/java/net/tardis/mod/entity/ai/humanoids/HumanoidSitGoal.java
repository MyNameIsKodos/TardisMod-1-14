package net.tardis.mod.entity.ai.humanoids;

import java.util.EnumSet;

import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.player.PlayerEntity;
import net.tardis.mod.entity.humanoid.TameableHumanoidEntity;

public class HumanoidSitGoal extends Goal {
   private final TameableHumanoidEntity humanoid;

   public HumanoidSitGoal(TameableHumanoidEntity entityIn) {
      this.humanoid = entityIn;
      this.setMutexFlags(EnumSet.of(Goal.Flag.JUMP, Goal.Flag.MOVE));
   }
   
   @Override
   public boolean shouldContinueExecuting() {
      return this.humanoid.isSitting();
   }

   @Override
   public boolean shouldExecute() {
      if (!this.humanoid.isTamed() || this.humanoid.isInWaterOrBubbleColumn() || !this.humanoid.isOnGround()) {
         return false;
      } else {
         PlayerEntity player = this.humanoid.getOwnerEntity();
         if (player == null) {
            return true;
         } else {
            return this.humanoid.getDistanceSq(player) < 144.0D && player.getRevengeTarget() != null ? false : this.humanoid.isSitting();
         }
      }
   }

   @Override
   public void startExecuting() {
      this.humanoid.getNavigator().clearPath();
      this.humanoid.setSleepingPose(true);
   }

   @Override
   public void resetTask() {
      this.humanoid.setSleepingPose(false);
   }
}