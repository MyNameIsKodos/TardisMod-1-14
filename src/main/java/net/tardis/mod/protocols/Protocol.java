package net.tardis.mod.protocols;

import javax.annotation.Nullable;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Util;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.tileentities.ConsoleTile;

public abstract class Protocol extends ForgeRegistryEntry<Protocol>{
	@Nullable
	private String translationKey;
	
	public abstract void call(World world, PlayerEntity player, ConsoleTile console);
	
	public String getTranslationKey() {
		if (this.translationKey == null) {
			this.translationKey = Util.makeTranslationKey("protocol", this.getRegistryName());
		}
		return this.translationKey;
	}
	
	public TranslationTextComponent getDisplayName(ConsoleTile tile) {
		return new TranslationTextComponent(this.getTranslationKey());
	}

	/** Determines if this protocol should be added to a console tile*/
	public boolean shouldBeAdded(ConsoleTile tile) {
		return true;
	}

    /** Gets the name of the submenu*/
	public String getSubmenu() {
		return "main";
	}

}
