package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction.Axis;
import net.minecraft.util.math.vector.Vector3d;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.registries.ControlRegistry.ControlEntry;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnellConsoleTile;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class ZControl extends AxisControl{
	
	public ZControl(ControlEntry entry, ConsoleTile console, ControlEntity entity) {
		super(entry, console, entity, Axis.Z);
	}

	@Override
	public EntitySize getSize() {
		if (this.getConsole() instanceof ToyotaConsoleTile){
			return EntitySize.flexible(0.0625F, 0.0625F);
		}
		if(getConsole() instanceof CoralConsoleTile)
			return EntitySize.flexible(0.0625F, 0.0625F);

		return EntitySize.flexible(0.0625F, 0.0625F);
	}

	@Override
	public Vector3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vector3d(-6 / 16.0, 10 / 16.0, 6 / 16.0);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vector3d(-0.4059090875328474, 0.625, 0.3479865154450874);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vector3d(0.22546609302967927, 0.5, -0.7);
		}
		
		if(this.getConsole() instanceof HartnellConsoleTile)
			return new Vector3d(-0.44, 0.5, 0.37);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vector3d(0.08, 0.6, 0.508);
		
		if (this.getConsole() instanceof XionConsoleTile)
			return new Vector3d(0.74, 0.4, 0.2924539413501982);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return new Vector3d(0.8132539480909782, 0.46875, 0.737394527955187);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return new Vector3d(0.759197527988781, 0.4375, -0.5713981345208767);
		
		
		return new Vector3d(-8.85 / 16.0, 9.85 / 16.0, -3.35 / 16.0);
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}
}
