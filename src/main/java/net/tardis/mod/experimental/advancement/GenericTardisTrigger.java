package net.tardis.mod.experimental.advancement;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gson.JsonObject;

import net.minecraft.advancements.ICriterionTrigger;
import net.minecraft.advancements.PlayerAdvancements;
import net.minecraft.advancements.criterion.AbstractCriterionTrigger;
import net.minecraft.advancements.criterion.CriterionInstance;
import net.minecraft.advancements.criterion.EntityPredicate;
import net.minecraft.advancements.criterion.EntityPredicate.AndPredicate;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.loot.ConditionArrayParser;
import net.minecraft.util.ResourceLocation;

public class GenericTardisTrigger extends AbstractCriterionTrigger<GenericTardisTrigger.Instance> {
    private final ResourceLocation RL;

    /**
     * Instantiates a new custom trigger.
     *
     * @param parString the par string
     */
    public GenericTardisTrigger(String parString) {
        RL = new ResourceLocation(parString);
    }

    /**
     * Instantiates a new custom trigger.
     *
     * @param parRL the par RL
     */
    public GenericTardisTrigger(ResourceLocation parRL) {
        RL = parRL;
    }

    /*
     * (non-Javadoc)
     * @see net.minecraft.advancements.ICriterionTrigger#getId()
     */
    @Override
    public ResourceLocation getId() {
        return RL;
    }

    /**
     * Deserialize a ICriterionInstance of this trigger from the data in the JSON.
     *
     * @param object    the json
     * @param entityPredicate the context
     * @return the tame bird trigger. instance
     */
	@Override
	public Instance deserializeTrigger(JsonObject object, AndPredicate entityPredicate,
	        ConditionArrayParser conditionsParser) {
		EntityPredicate.AndPredicate entitypredicate$andpredicate = EntityPredicate.AndPredicate.deserializeJSONObject(object, "sub_condition", conditionsParser);
    	return new GenericTardisTrigger.Instance(getId(), entitypredicate$andpredicate);
	}
    
    
    /**
     * Trigger.
     *
     * @param player -  the player
     */
    public void test(ServerPlayerEntity player) {
    	this.triggerListeners(player, (instance) -> {
            return instance.test(player);
         });
    }

	public static class Instance extends CriterionInstance {

        /**
         * Instantiates a new instance.
         *
         * @param parRL the par RL
         */
        public Instance(ResourceLocation parRL, EntityPredicate.AndPredicate playerCondition) {
            super(parRL, playerCondition);
        }

        /**
         * Test.
         *
         * @return true, if successful
         */
        public boolean test(ServerPlayerEntity player) {
            return true;
        }
        
        
    }
    
//    public static class Listeners {
//        private final PlayerAdvancements playerAdvancements;
//        private final Set<ICriterionTrigger.Listener<GenericTardisTrigger.Instance>> listeners = Sets.newHashSet();
//
//        /**
//         * Instantiates a new listeners.
//         *
//         * @param playerAdvancementsIn the player advancements in
//         */
//        public Listeners(PlayerAdvancements playerAdvancementsIn) {
//            playerAdvancements = playerAdvancementsIn;
//        }
//
//        /**
//         * Checks if is empty.
//         *
//         * @return true, if is empty
//         */
//        public boolean isEmpty() {
//            return listeners.isEmpty();
//        }
//
//        /**
//         * Adds the listener.
//         *
//         * @param listener the listener
//         */
//        public void add(ICriterionTrigger.Listener<GenericTardisTrigger.Instance> listener) {
//            listeners.add(listener);
//        }
//
//        /**
//         * Removes the listener.
//         *
//         * @param listener the listener
//         */
//        public void remove(ICriterionTrigger.Listener<GenericTardisTrigger.Instance> listener) {
//            listeners.remove(listener);
//        }
//
//        /**
//         * Trigger.
//         *
//         * @param player the player
//         */
//        public void trigger(ServerPlayerEntity player) {
//            ArrayList<ICriterionTrigger.Listener<GenericTardisTrigger.Instance>> list = null;
//
//            for (ICriterionTrigger.Listener<GenericTardisTrigger.Instance> listener : listeners) {
//                if (listener.getCriterionInstance().test(player)) {
//                    if (list == null) {
//                        list = Lists.newArrayList();
//                    }
//
//                    list.add(listener);
//                }
//            }
//
//            if (list != null) {
//                for (ICriterionTrigger.Listener<GenericTardisTrigger.Instance> listener1 : list) {
//                    listener1.grantCriterion(playerAdvancements);
//                }
//            }
//        }
//    }
	
}