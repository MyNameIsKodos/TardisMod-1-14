package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.IWaterLoggable;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.misc.VoxelShapeDirectional;
import net.tardis.mod.blocks.template.NotSolidTileBlock;
import net.tardis.mod.containers.SpectrometerContainer;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.tileentities.machines.NeutronicSpectrometerTile;

import javax.annotation.Nullable;

public class NeutronicSpectrometerBlock extends NotSolidTileBlock implements IWaterLoggable {

    public static final VoxelShapeDirectional SHAPE = new VoxelShapeDirectional(makeShape());

    public NeutronicSpectrometerBlock() {
        super(Prop.Blocks.BASIC_TECH.get());
        this.setDefaultState(this.getDefaultState().with(BlockStateProperties.WATERLOGGED, false));
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return SHAPE.getFor(state.get(BlockStateProperties.HORIZONTAL_FACING));
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        super.fillStateContainer(builder);
        builder.add(BlockStateProperties.HORIZONTAL_FACING, BlockStateProperties.WATERLOGGED);
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return super.getStateForPlacement(context).with(BlockStateProperties.HORIZONTAL_FACING, context.getPlacementHorizontalFacing().getOpposite());
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {

        TileEntity te = worldIn.getTileEntity(pos);
        if(!worldIn.isRemote && te instanceof NeutronicSpectrometerTile){
        	NetworkHooks.openGui((ServerPlayerEntity) player, new INamedContainerProvider() {

                @Override
                public Container createMenu(int id, PlayerInventory playerInv, PlayerEntity ent) {
                    return new SpectrometerContainer(id, playerInv, (NeutronicSpectrometerTile)te);
                }

                @Override
                public ITextComponent getDisplayName() {
                    return new TranslationTextComponent("container." + Tardis.MODID + ".spectrometer");
                }
            }, buf -> buf.writeBlockPos(pos));
        }

        return ActionResultType.SUCCESS;
    }

    public static VoxelShape makeShape(){
        VoxelShape shape = VoxelShapes.empty();
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.0625, 0, 0.0625, 0.9375, 0.0625, 0.9375), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.0625, 0.5625, 0.0625, 0.9375, 1, 0.9375), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.0625, 0.0625, 0.875, 0.9375, 0.5625, 0.9375), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.875, 0.0625, 0.375, 0.9375, 0.5625, 0.875), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.0625, 0.0625, 0.375, 0.125, 0.5625, 0.875), IBooleanFunction.OR);

        return shape;
    }
}
