package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.SoundType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;

public class FloorClutterBlock extends Block{

    public static final VoxelShape SHAPE = Block.makeCuboidShape(0, 0, 0, 16, 0.25, 16);

    public FloorClutterBlock(Properties prop, SoundType sound, float hardness, float resistance) {

        super(prop.sound(sound).hardnessAndResistance(hardness, resistance).notSolid());
    }


    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return SHAPE;
    }
}
