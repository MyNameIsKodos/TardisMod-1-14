package net.tardis.mod.world.dimensions;

import javax.annotation.Nullable;

import net.minecraft.client.world.DimensionRenderInfo;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.tardis.mod.client.renderers.sky.MoonSkyRenderer;

@OnlyIn(Dist.CLIENT) //This is one of the few cases where we can be using OnlyIn - Vanilla uses this is a marker hack to strip bytecode from one side (server or client)
public class SpaceSkyProperty extends DimensionRenderInfo {

    public static final float[] SUNSET_COLORS = {0, 0, 0, 0};

	public SpaceSkyProperty() {
		this(Float.NaN, false, FogType.NONE, true, false);
	}
	
    public SpaceSkyProperty(float cloudLevel, boolean hasGround, FogType skyType, boolean forceBrightLightmap, boolean constantAmbientLight) {
        super(cloudLevel, hasGround, skyType, forceBrightLightmap, constantAmbientLight);
        this.setSkyRenderHandler(new MoonSkyRenderer());
    }

    //adjustSkyColor
    @Override
    public Vector3d func_230494_a_(Vector3d color, float sunHeight) {
    	return new Vector3d(0,0,0);
    }

    //isFoggyAt
    @Override
    public boolean func_230493_a_(int camX, int camY) {
        return false;
    }

    @Nullable
    @Override
    public float[] func_230492_a_(float p_230492_1_, float p_230492_2_) {
        return SUNSET_COLORS;
    }
}