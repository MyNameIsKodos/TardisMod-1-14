package net.tardis.mod.registries;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import com.google.common.collect.Lists;

import net.minecraft.util.Direction.Axis;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;
import net.tardis.mod.flight.BulkheadFlightEvent;
import net.tardis.mod.flight.CrashEvent;
import net.tardis.mod.flight.DimensionFlightEvent;
import net.tardis.mod.flight.DirectionalFlightEvent;
import net.tardis.mod.flight.FlightEvent;
import net.tardis.mod.flight.FlightEventFactory;
import net.tardis.mod.flight.FlightEventFactory.IFlightEventFactory;
import net.tardis.mod.flight.RefuelerFlightEvent;
import net.tardis.mod.flight.ResidualArtronEvent;
import net.tardis.mod.flight.ScrapFlightEvent;
import net.tardis.mod.flight.TardisCollideInstigate;
import net.tardis.mod.flight.TardisCollideRecieve;
import net.tardis.mod.flight.TimeWindFlightEvent;
import net.tardis.mod.flight.VerticalFlightEvent;
import net.tardis.mod.misc.rng.RarityPool;

public class FlightEventRegistry {

    public static final RarityPool<FlightEventFactory> RANDOM_EVENTS = new RarityPool<>();

    public static final DeferredRegister<FlightEventFactory> FLIGHT_EVENTS = DeferredRegister.create(FlightEventFactory.class, Tardis.MODID);
    
    public static Supplier<IForgeRegistry<FlightEventFactory>> FLIGHT_EVENT_REGISTRY = FLIGHT_EVENTS.makeRegistry("flight_event", () -> new RegistryBuilder<FlightEventFactory>().setMaxID(Integer.MAX_VALUE - 1));
    
    public static final RegistryObject<FlightEventFactory> SCRAP = FLIGHT_EVENTS.register("scrap", () -> setupFlightEvent(ScrapFlightEvent::new, () -> Lists.newArrayList(ControlRegistry.RANDOM.get().getRegistryName(), ControlRegistry.FACING.get().getRegistryName())));

    public static final RegistryObject<FlightEventFactory> X = FLIGHT_EVENTS.register("x", () -> setupFlightEvent((entry, keys) -> new DirectionalFlightEvent(entry, Axis.X, keys), () -> Lists.newArrayList(ControlRegistry.X.get().getRegistryName())));
    public static final RegistryObject<FlightEventFactory> Y = FLIGHT_EVENTS.register("y", () -> setupFlightEvent((entry, keys) -> new DirectionalFlightEvent(entry, Axis.Y, keys), () -> Lists.newArrayList(ControlRegistry.Y.get().getRegistryName())));
    public static final RegistryObject<FlightEventFactory> Z = FLIGHT_EVENTS.register("z", () -> setupFlightEvent((entry, keys) -> new DirectionalFlightEvent(entry, Axis.Z, keys), () -> Lists.newArrayList(ControlRegistry.Z.get().getRegistryName())));

    public static final RegistryObject<FlightEventFactory> TIMEWIND = FLIGHT_EVENTS.register("time_wind", () -> setupFlightEvent(TimeWindFlightEvent::new, () -> Lists.newArrayList(ControlRegistry.THROTTLE.get().getRegistryName())));

    public static final RegistryObject<FlightEventFactory> BULKHEAD = FLIGHT_EVENTS.register("bulkhead", () -> setupFlightEvent(BulkheadFlightEvent::new, () -> Lists.newArrayList(ControlRegistry.DOOR.get().getRegistryName())));

    public static final RegistryObject<FlightEventFactory> REFUELER = FLIGHT_EVENTS.register("refueler", () -> setupFlightEvent(RefuelerFlightEvent::new, () -> Lists.newArrayList(ControlRegistry.REFUELER.get().getRegistryName())));

    public static final RegistryObject<FlightEventFactory> DIMENSION = FLIGHT_EVENTS.register("dimension", () -> setupFlightEvent(DimensionFlightEvent::new, () -> Lists.newArrayList(ControlRegistry.DIMENSION.get().getRegistryName())));

    public static final RegistryObject<FlightEventFactory> VERTICAL = FLIGHT_EVENTS.register("vertical", () -> setupFlightEvent(VerticalFlightEvent::new, () -> Lists.newArrayList(ControlRegistry.LAND_TYPE.get().getRegistryName())));

    public static final RegistryObject<FlightEventFactory> COLLIDE_INSTIGATE = FLIGHT_EVENTS.register("collide_instigate", () -> setupFlightEvent(TardisCollideInstigate::new, TardisCollideInstigate.CONTROLS, false));

    public static final RegistryObject<FlightEventFactory> COLLIDE_RECIEVE = FLIGHT_EVENTS.register("collide_recieve", () -> setupFlightEvent(TardisCollideRecieve::new, TardisCollideRecieve.CONTROLS, false));

    public static final RegistryObject<FlightEventFactory> CRASH_EVENT = FLIGHT_EVENTS.register("crash", () -> setupFlightEvent(CrashEvent::new, CrashEvent.CONTROLS, false));

    public static final RegistryObject<FlightEventFactory> RESIDUAL_ARTRON = FLIGHT_EVENTS.register("residual_artron", () -> setupFlightEvent(ResidualArtronEvent::new, () -> Lists.newArrayList(ControlRegistry.REFUELER.get().getRegistryName(), ControlRegistry.FACING.get().getRegistryName())));
    
    @Nullable
    public static FlightEventFactory getRandomEvent(Random r) {
    	return RANDOM_EVENTS.chooseRandom(r);
    }
    
    public static FlightEventFactory setupFlightEvent(IFlightEventFactory<FlightEvent> event, Supplier<ArrayList<ResourceLocation>> sequence){
        FlightEventFactory fact = new FlightEventFactory(event, sequence);
        return fact;
    }
    
    public static FlightEventFactory setupFlightEvent(IFlightEventFactory<FlightEvent> event, Supplier<ArrayList<ResourceLocation>> sequence, boolean normal) {
        FlightEventFactory fact = setupFlightEvent(event, sequence);
        fact.setNormal(false);
        return fact;
    }

    //Call in server setup or FMLStartupEvent
    public static void addRandomEvent(int weight, FlightEventFactory event){
        RANDOM_EVENTS.addChance(weight, event);
    }

    public static void registerRandomEntries() {
        //Add Common
        addRandomEvent(25, X.get());
        addRandomEvent(25, Y.get());
        addRandomEvent(25, Z.get());

        //Add less common
        addRandomEvent(20, VERTICAL.get());
        addRandomEvent(20, TIMEWIND.get());
        addRandomEvent(10, REFUELER.get());

        //Add rare
        addRandomEvent(10, BULKHEAD.get());
        addRandomEvent(10, SCRAP.get());
        addRandomEvent(10, DIMENSION.get());

        //Add blue rare
        addRandomEvent(5, RESIDUAL_ARTRON.get());
    }
}
